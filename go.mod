module gitlab.ethz.ch/chgerber/MessageComposition

go 1.12

require (
	cloud.google.com/go v0.42.0 // indirect
	github.com/olivere/elastic v6.2.21+incompatible
	github.com/sirupsen/logrus v1.4.2
	github.com/stretchr/testify v1.3.0
	github.com/urfave/cli v1.20.0
	gitlab.ethz.ch/chgerber/annotation/v2 v2.2.0
	gitlab.ethz.ch/chgerber/elastic v1.0.3
	gitlab.ethz.ch/chgerber/monitor v0.0.0-20190527191251-2bb9dd731340
	gitlab.ethz.ch/chgerber/video v0.0.0-20190527150454-f0fa080cc6fa
	go.mongodb.org/mongo-driver v1.0.4
	golang.org/x/crypto v0.0.0-20191206172530-e9b2fee46413 // indirect
	golang.org/x/lint v0.0.0-20191125180803-fdd1cda4f05f // indirect
	golang.org/x/net v0.0.0-20191209160850-c0dbc17a3553 // indirect
	golang.org/x/sys v0.0.0-20191206220618-eeba5f6aabab // indirect
	golang.org/x/tools v0.0.0-20191206204035-259af5ff87bd // indirect
	golang.org/x/xerrors v0.0.0-20191204190536-9bdfabe68543 // indirect
	google.golang.org/grpc v1.22.0 // indirect
)
