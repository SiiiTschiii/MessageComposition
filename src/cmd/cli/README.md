## Composey Shell
Submit message composition queries from the command line.

#### Usage
Make sure env. variables `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY` are set to an account with access rights for the configured AWS Elasticsearch domain.


```bash
go run cli.go
-h
```
