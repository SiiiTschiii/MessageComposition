package main

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli"
	"gitlab.ethz.ch/chgerber/MessageComposition/src/pkg/composition"
	"gitlab.ethz.ch/chgerber/MessageComposition/src/pkg/display"
	"gitlab.ethz.ch/chgerber/MessageComposition/src/pkg/materialize"
	"gitlab.ethz.ch/chgerber/MessageComposition/src/pkg/score"
	"gitlab.ethz.ch/chgerber/MessageComposition/src/pkg/types"
	"os"
)

var es *types.Elastic

var n int // number of candidates to produce
var simScoreWeight float64
var numPartScoreWeight float64
var splitScoreWeight float64
var balancedScoreWeight float64
var popularityScoreWeight float64
var showNotCandidatesInBrowser bool
var plotNotCECofBestNCandidates bool

var index = "dev.playlists"
var playlist = "tbbt"

func prepare() error {

	var err error
	es, err = types.NewElastic(
		os.Getenv("ES_URL_TEST"),
		index,
	)

	if err != nil {
		return err
	}

	return nil
}

func messageComposition(phrase string) error {

	// 1. FIND PARTS
	classes, err := composition.FindParts(phrase, es, playlist)
	if err != nil {
		return err
	}

	log.Debugf("partClasses: %v", len(classes))

	// 2. COMPOSE
	cecs := composition.Compose(phrase, classes)
	log.Debugf("numClasses: %v", len(cecs))

	// 3. SCORING
	popScore := score.Popularity{W: popularityScoreWeight, LevelThresholds: []int{}}
	ref := score.NewReferee(
		&score.NumPart{
			W: numPartScoreWeight,
		},
		&score.Similarity{
			W:            simScoreWeight,
			StringMetric: score.NewLevenshteinWord()},
		&score.BalancedPart{
			W: balancedScoreWeight,
		},
		&score.Split{
			W: splitScoreWeight,
		},
		&popScore,
	)

	scoredCECs, candidates, err := materialize.RealizeTop(n, es, ref, cecs, popScore, playlist)
	if err != nil {
		return err
	}

	log.Debugf("numCandidate: %v", len(candidates))

	if !plotNotCECofBestNCandidates {
		err = display.PlotCECofBestNCandidates(scoredCECs, phrase, ref, popScore.Base())
		if err != nil {
			return err
		}
	}

	if !showNotCandidatesInBrowser {
		err = display.CandidatesInBrowser(candidates, ref)
		if err != nil {
			return err
		}
	}
	return nil
}

func main() {

	log.SetFormatter(&log.JSONFormatter{})
	log.SetLevel(log.DebugLevel)

	app := cli.NewApp()
	app.EnableBashCompletion = true
	app.Version = "0.1.0"
	app.Name = "Composey"
	app.Usage = "Compose Messages from Movies"
	app.Authors = []cli.Author{
		cli.Author{
			Name:  "Christof Gerber",
			Email: "christof.gerber1@gmail.com",
		},
	}

	app.Flags = []cli.Flag{
		cli.IntFlag{
			Name:        "n",
			Value:       100,
			Usage:       "Number of candidate results to produce",
			Destination: &n,
		},
		cli.Float64Flag{
			Name:        "similarity-weight, sim",
			Value:       20.0,
			Usage:       "Similarity Score Weight",
			Destination: &simScoreWeight,
		},
		cli.Float64Flag{
			Name:        "num-part-weight, num",
			Value:       2.0,
			Usage:       "Number of Parts Score Weight",
			Destination: &numPartScoreWeight,
		},
		cli.Float64Flag{
			Name:        "split-weight, split",
			Value:       10.0,
			Usage:       "Split Score Weight",
			Destination: &splitScoreWeight,
		},
		cli.Float64Flag{
			Name:        "balanced-part-weight, bal",
			Value:       1.0,
			Usage:       "Balanced Part Score Weight",
			Destination: &balancedScoreWeight,
		},
		cli.Float64Flag{
			Name:        "popularity-weight, pop",
			Value:       1.0,
			Usage:       "Popularity Score Weight",
			Destination: &popularityScoreWeight,
		},
		cli.BoolFlag{
			Name:        "no-print-cand",
			Usage:       "Don't display the candidates in browser",
			Destination: &showNotCandidatesInBrowser,
		},
		cli.BoolFlag{
			Name:        "no-print-cec",
			Usage:       "Don't display a plot of the CEC accounting for the n candidates",
			Destination: &plotNotCECofBestNCandidates,
		},
	}

	app.Action = func(c *cli.Context) error {
		fmt.Printf("Hello I am Arni!\n")
		return nil
	}

	app.Commands = []cli.Command{
		{
			Name:    "query",
			Aliases: []string{"q"},
			Usage:   "composes `PHRASE` from movie subtitles",
			Action: func(c *cli.Context) error {
				err := messageComposition(c.Args().First())
				if err != nil {
					log.Fatal(err)
				}
				return nil
			},
		},
	}

	fmt.Println("Hello to the Composey CLI")

	fmt.Println("Configured Index: " + index)
	// Load docs and idx
	err := prepare()
	if err != nil {
		log.Fatal(err)
	}

	// queries

	err = app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}

}
