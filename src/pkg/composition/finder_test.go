package composition

import (
	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"gitlab.ethz.ch/chgerber/MessageComposition/src/pkg/types"
	"gitlab.ethz.ch/chgerber/MessageComposition/src/pkg/util"
	"gitlab.ethz.ch/chgerber/annotation/v2"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"os"
	"strings"
	"testing"
	"time"
)

func createTestAnnotations(texts ...string) (as []*annotation.Annotation) {

	for _, text := range texts {
		a := &annotation.Annotation{
			ID: primitive.NewObjectID(),
			Subtitle: annotation.Subtitle{
				Text: text,
			},
			Playlist: annotation.Playlist{
				ID:   "test",
				Name: "test",
			},
		}
		as = append(as, a)
	}

	return as
}

func TestGetEquivalenceClasses(t *testing.T) {
	query := "my name is arni"
	classes := computePartEquivalenceClasses(query)
	for _, class := range classes {
		var s []string
		for i := class.StartIDX; i <= class.EndIDX; i++ {
			s = append(s, strings.Fields(query)[i])
		}
		if s := util.WordsToString(s); s != class.Text {
			t.Errorf("Expected '%s' got '%s'", class.Text, s)
		}
	}

	assert.Equal(t, 10, len(classes))
}

func TestFindEquivalenceClasses_AllClassesInOne(t *testing.T) {

	annos := createTestAnnotations(
		"my name is arni it really is",
		"you cannot hide you",
		"this is eth zurich",
	)

	es, err := types.NewElastic(os.Getenv("ES_URL_TEST"), strings.ToLower(t.Name())+primitive.NewObjectID().Hex())
	if err != nil {
		t.Fatal(err)
	}

	defer es.DeleteIndex()

	err = es.AddAnnotations(annos)
	if err != nil {
		t.Fatal(err)
	}

	time.Sleep(2 * time.Second)

	query := "my name is arni"
	availableClasses, err := FindParts(query, es, "test")
	if err != nil {
		t.Fatal(err)
	}

	expI := 10
	if is := len(availableClasses); is != expI {
		t.Errorf("Expected %v got %v", expI, is)
	}

	for _, class := range availableClasses {
		var s []string
		for i := class.StartIDX; i <= class.EndIDX; i++ {
			s = append(s, strings.Fields(query)[i])
		}
		if s := util.WordsToString(s); s != class.Text {
			t.Errorf("Expected '%s' got '%s'", class.Text, s)
		}
	}
	/*
		for text, posting := range postings {
			fmt.Printf("%s : docId: %v positions: %v\n", text, posting[0].DocID(), posting[0].Positions())
		}
	*/

}

func TestFindEquivalenceClasses_AllClassesSpread(t *testing.T) {

	annos := createTestAnnotations(
		"my name was christof",
		"you cannot hide that this is never",
		"hello is arni here",
	)

	es, err := types.NewElastic(os.Getenv("ES_URL_TEST"), strings.ToLower(t.Name())+primitive.NewObjectID().Hex())
	if err != nil {
		t.Fatal(err)
	}

	defer es.DeleteIndex()

	err = es.AddAnnotations(annos)
	if err != nil {
		t.Fatal(err)
	}

	time.Sleep(2 * time.Second)

	query := "my name is arni"
	availableClasses, err := FindParts(query, es, "test")
	if err != nil {
		t.Fatal(err)
	}

	expI := 6
	if is := len(availableClasses); is != expI {
		t.Errorf("Expected %v got %v", expI, is)
	}

	for _, class := range availableClasses {
		var s []string
		//fmt.Printf("%s %v %v\n", class.Text(), class.StartIDXinSub(), class.EndIDX())
		for i := class.StartIDX; i <= class.EndIDX; i++ {
			s = append(s, strings.Fields(query)[i])
		}
		if s := util.WordsToString(s); s != class.Text {
			t.Errorf("Expected '%s' got '%s'", class.Text, s)
		}
	}
}

func TestFindEquivalenceClasses_RepeatingTerms(t *testing.T) {

	annos := createTestAnnotations(
		"my name was christof",
		"you cannot hide that this is never",
		"hello is arni here",
	)

	es, err := types.NewElastic(os.Getenv("ES_URL_TEST"), strings.ToLower(t.Name())+primitive.NewObjectID().Hex())
	if err != nil {
		t.Fatal(err)
	}

	defer es.DeleteIndex()

	err = es.AddAnnotations(annos)
	if err != nil {
		t.Fatal(err)
	}

	time.Sleep(2 * time.Second)

	query := "is my name is"
	availableClasses, err := FindParts(query, es, "test")
	if err != nil {
		t.Fatal(err)
	}

	expI := 5
	if is := len(availableClasses); is != expI {
		t.Errorf("Expected %v got %v", expI, is)
	}

	for _, class := range availableClasses {
		//fmt.Printf("%s %v %v\n", class.Text(), class.StartIDXinSub(), class.EndIDX())
		var s []string
		for i := class.StartIDX; i <= class.EndIDX; i++ {
			s = append(s, strings.Fields(query)[i])
		}
		if s := util.WordsToString(s); s != class.Text {
			t.Errorf("Expected '%s' got '%s'", class.Text, s)
		}

	}
}

func TestFindEquivalenceClasses_SplitInformation(t *testing.T) {

	annos := createTestAnnotations(
		"my name is arni it really is",
		"you cannot hide you",
		"this is eth zurich",
		"happy birthday to you",
		"happy birthday to you arni",
	)

	es, err := types.NewElastic(os.Getenv("ES_URL_TEST"), strings.ToLower(t.Name())+primitive.NewObjectID().Hex())
	if err != nil {
		t.Fatal(err)
	}

	defer es.DeleteIndex()

	err = es.AddAnnotations(annos)
	if err != nil {
		t.Fatal(err)
	}

	time.Sleep(2 * time.Second)

	query := "my name is arni"
	availableClasses, err := FindParts(query, es, "test")
	if err != nil {
		t.Fatal(err)
	}

	for _, class := range availableClasses {
		isRange := class.Splits()
		expRange := types.SplitRange{1, 1}
		if isRange != expRange {
			t.Errorf("Expected %+v got %+v, %s", expRange, isRange, class.Text)
		}
		expI := 0
		if is := class.NotSplitPartFreq; is != expI {
			t.Errorf("Expected %v got %v, %s", expI, is, class.Text)
		}
	}

	query = "you cannot hide you"
	availableClasses, err = FindParts(query, es, "test")
	if err != nil {
		t.Fatal(err)
	}

	for _, class := range availableClasses {
		isRange := class.Splits()
		if class.Text == "you cannot hide you" {
			expRange := types.SplitRange{0, 0}
			if isRange != expRange {
				t.Errorf("Expected %+v got %+v, %s", expRange, isRange, class.Text)
			}
			expI := 0
			if is := class.SplitPartFreq; is != expI {
				t.Errorf("Expected %v got %v, %s", expI, is, class.Text)
			}
		} else {
			expRange := types.SplitRange{1, 1}
			if isRange != expRange {
				t.Errorf("Expected %+v got %+v, %s", expRange, isRange, class.Text)
			}
		}
	}

	query = "happy birthday to you"

	availableClasses, err = FindParts(query, es, "test")
	if err != nil {
		t.Fatal(err)
	}

	for _, class := range availableClasses {
		isRange := class.Splits()
		if class.Text == "happy birthday to you" {
			expRange := types.SplitRange{0, 1}
			if isRange != expRange {
				t.Errorf("Expected %+v got %+v, %s", expRange, isRange, class.Text)
			}
			expI := 1
			if is := class.SplitPartFreq; is != expI {
				t.Errorf("Expected %v got %v, %s", expI, is, class.Text)
			}
			expI = 1
			if is := class.NotSplitPartFreq; is != expI {
				t.Errorf("Expected %v got %v, %s", expI, is, class.Text)
			}
		} else {
			expRange := types.SplitRange{1, 1}
			if isRange != expRange {
				t.Errorf("Expected %+v got %+v, %s", expRange, isRange, class.Text)
			}
		}
	}
}

func benchmarkFindParts(query string, b *testing.B) {

	es, err := types.NewElastic(
		os.Getenv("ES_URL_TEST"),
		"dev.1m2",
	)

	if err != nil {
		b.Fatal(err)
	}

	b.ResetTimer()

	for n := 0; n < b.N; n++ {

		// get all pecs and their equivalence class
		pec, err = FindParts(query, es, "test")
		if err != nil {
			b.Fatal(err)
		}

	}

	log.Debugf("#pec: %v", len(pec))

}

func BenchmarkFindParts1(b *testing.B) {
	benchmarkFindParts(util.WordsToString(strings.Fields(util.BenchmarkQuery)[:1]), b)
}
func BenchmarkFindParts2(b *testing.B) {
	benchmarkFindParts(util.WordsToString(strings.Fields(util.BenchmarkQuery)[:2]), b)
}
func BenchmarkFindParts3(b *testing.B) {
	benchmarkFindParts(util.WordsToString(strings.Fields(util.BenchmarkQuery)[:3]), b)
}
func BenchmarkFindParts4(b *testing.B) {
	benchmarkFindParts(util.WordsToString(strings.Fields(util.BenchmarkQuery)[:4]), b)
}
func BenchmarkFindParts5(b *testing.B) {
	benchmarkFindParts(util.WordsToString(strings.Fields(util.BenchmarkQuery)[:5]), b)
}
func BenchmarkFindParts6(b *testing.B) {
	benchmarkFindParts(util.WordsToString(strings.Fields(util.BenchmarkQuery)[:6]), b)
}
func BenchmarkFindParts7(b *testing.B) {
	benchmarkFindParts(util.WordsToString(strings.Fields(util.BenchmarkQuery)[:7]), b)
}
func BenchmarkFindParts8(b *testing.B) {
	benchmarkFindParts(util.WordsToString(strings.Fields(util.BenchmarkQuery)[:8]), b)
}
func BenchmarkFindParts9(b *testing.B) {
	benchmarkFindParts(util.WordsToString(strings.Fields(util.BenchmarkQuery)[:9]), b)
}
func BenchmarkFindParts10(b *testing.B) {
	benchmarkFindParts(util.WordsToString(strings.Fields(util.BenchmarkQuery)[:10]), b)
}
func BenchmarkFindParts11(b *testing.B) {
	benchmarkFindParts(util.WordsToString(strings.Fields(util.BenchmarkQuery)[:11]), b)
}
func BenchmarkFindParts12(b *testing.B) {
	benchmarkFindParts(util.WordsToString(strings.Fields(util.BenchmarkQuery)[:12]), b)
}
func BenchmarkFindParts13(b *testing.B) {
	benchmarkFindParts(util.WordsToString(strings.Fields(util.BenchmarkQuery)[:13]), b)
}
func BenchmarkFindParts14(b *testing.B) {
	benchmarkFindParts(util.WordsToString(strings.Fields(util.BenchmarkQuery)[:14]), b)
}
func BenchmarkFindParts15(b *testing.B) {
	benchmarkFindParts(util.WordsToString(strings.Fields(util.BenchmarkQuery)[:15]), b)
}
func BenchmarkFindParts16(b *testing.B) {
	benchmarkFindParts(util.WordsToString(strings.Fields(util.BenchmarkQuery)[:16]), b)
}
func BenchmarkFindParts17(b *testing.B) {
	benchmarkFindParts(util.WordsToString(strings.Fields(util.BenchmarkQuery)[:17]), b)
}
func BenchmarkFindParts18(b *testing.B) {
	benchmarkFindParts(util.WordsToString(strings.Fields(util.BenchmarkQuery)[:18]), b)
}
func BenchmarkFindParts19(b *testing.B) {
	benchmarkFindParts(util.WordsToString(strings.Fields(util.BenchmarkQuery)[:19]), b)
}
func BenchmarkFindParts20(b *testing.B) {
	benchmarkFindParts(util.WordsToString(strings.Fields(util.BenchmarkQuery)[:20]), b)
}

// TODO: test if some classes are missing
// TODO: test if double occurences in subtitles are seen as own equivalence classes (requires implementaion in index query frist)
