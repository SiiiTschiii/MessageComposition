package composition

import (
	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"gitlab.ethz.ch/chgerber/MessageComposition/src/pkg/types"
	"gitlab.ethz.ch/chgerber/MessageComposition/src/pkg/util"
	"math"
	"math/big"
	"time"

	"os"
	"strings"
	"testing"
)

func TestMain(m *testing.M) {

	log.SetLevel(log.WarnLevel)

	// run all tests and exit
	os.Exit(m.Run())
}

func TestBase3Range(t *testing.T) {
	min, max := base3Range(4)
	if min != 27 || max != 80 {
		t.Errorf("base3Range computes wrong range")
	}
}

func TestFindParts(t *testing.T) {
	phrase := "my name is arni"
	other := "my name is"
	classes := computePartEquivalenceClasses(phrase)
	classesOther := computePartEquivalenceClasses(other)
	parts := partsAvailable(classes, classes)
	if parts == nil {
		t.Errorf("class not found although expected to be found")
	}
	parts = partsAvailable(classes, classesOther)
	if parts != nil {
		t.Errorf("All classes found which should not have been the case")
	}
	parts = partsAvailable(classesOther, classes)
	if parts == nil {
		t.Errorf("class not found although expected to be found")
	}
}

func TestCompose_SplitsAndFreq(t *testing.T) {

	annos := createTestAnnotations(
		"my name is arni it really is",
		"you cannot hide you",
		"this is eth zurich",
		"happy birthday to you",
		"happy birthday to you arni",
		"happy birthday",
		"to you walt",
	)

	es, err := types.NewElastic(os.Getenv("ES_URL_TEST"), strings.ToLower(t.Name()))
	if err != nil {
		t.Fatal(err)
	}

	defer es.DeleteIndex()

	err = es.AddAnnotations(annos)
	if err != nil {
		t.Fatal(err)
	}

	time.Sleep(2 * time.Second)

	// target message to compose
	phrase := "happy birthday to you walt"

	// get all pecs and their equivalence class
	classes, err := FindParts(phrase, es, "test")
	if err != nil {
		t.Fatal(err)
	}

	// compose all possible candidate equivalence classes with the available part equivalence classes
	candidateClasses := Compose(phrase, classes)

	// print candidate equivalence classes in base3 representation
	for _, candidateClass := range candidateClasses {
		if candidateClass.Base3() == "10002" {
			expRange := types.SplitRange{0, 1}
			if is := candidateClass.Splits(); is != expRange {
				t.Errorf("expected splitRange %+v, got %+v ", expRange, is)
			}
			// Test SplitFreq()
			expI := 1
			if is := len(candidateClass.SplitFreq()); is != expI {
				t.Errorf("expected splitRange %v, got %v ", expI, is)
			}
			expI = 2
			if is := len(candidateClass.SplitFreq()[0]); is != expI {
				t.Errorf("expected splitRange %v, got %v ", expI, is)
			}
			expM := [][]int{{1, 1}}
			if is := candidateClass.SplitFreq()[0][0]; is != expM[0][0] {
				t.Errorf("expected splitRange %v, got %v ", expM, is)
			}
			if is := candidateClass.SplitFreq()[0][1]; is != expM[0][1] {
				t.Errorf("expected splitRange %v, got %v ", expM, is)
			}
		}
		if candidateClass.Base3() == "10100" {
			expRange := types.SplitRange{0, 1}
			if is := candidateClass.Splits(); is != expRange {
				t.Errorf("expected splitRange %+v, got %+v ", expRange, is)

			}
			expI := 2
			if is := len(candidateClass.PartFreq()); is != expI {
				t.Errorf("expected %v, got %v ", expI, is)

			}
			expI = 3
			if is := candidateClass.PartFreq()[0]; is != expI {
				t.Errorf("expected %v, got %v ", expI, is)

			}
			expI = 1
			if is := candidateClass.PartFreq()[1]; is != expI {
				t.Errorf("expected %v, got %v ", expI, is)

			}
		}
		/*
			fmt.Printf("%s, splits: %+v, freq: %v \n", candidateClass.Base3(), candidateClass.SplitScore(), candidateClass.PartFreq())
		*/
	}
}

func TestZeroString(t *testing.T) {
	expS := "0000"
	if is := util.ZeroString(4); is != expS {
		t.Errorf("expected %s, got %s ", expS, is)
	}
}

func TestAddPrefix(t *testing.T) {
	exp1 := "00001"
	exp2 := "00000"
	if is := util.AddPrefix("00", []string{"001", "000"}); is[0] != exp1 || is[1] != exp2 {
		t.Errorf("expected %s, got %s ", exp1, is)
	}
}

func TestSplitLocationCombinations(t *testing.T) {
	n := 15
	k := 3
	expI := big.NewInt(0)
	expI.Binomial(int64(n), int64(k))
	is := util.BinaryNumbersWithKones(k, n)
	if expI.Cmp(big.NewInt(int64(len(is)))) != 0 {
		t.Errorf("expected %v, got %v ", expI, len(is))
	}
}

func TestNumRealizationNSplits(t *testing.T) {

	annos := createTestAnnotations(
		"my name is arni it really is",
		"you cannot hide you",
		"this is eth zurich",
		"happy birthday to you",
		"happy birthday to you arni",
		"happy birthday",
		"to you walt",
	)

	es, err := types.NewElastic(os.Getenv("ES_URL_TEST"), strings.ToLower(t.Name()))
	if err != nil {
		t.Fatal(err)
	}

	defer es.DeleteIndex()

	err = es.AddAnnotations(annos)
	if err != nil {
		t.Fatal(err)
	}

	time.Sleep(2 * time.Second)

	// target message to compose
	phrase := "happy birthday to you walt"

	// get all pecs and their equivalence class
	classes, err := FindParts(phrase, es, "test")
	if err != nil {
		t.Fatal(err)
	}

	// compose all possible candidate equivalence classes with the available part equivalence classes
	candidateClasses := Compose(phrase, classes)

	for _, candidateClass := range candidateClasses {
		for i := candidateClass.Splits().Min; i <= candidateClass.Splits().Max; i++ {
			//TODO How to test TestNumRealizationNSplits proprly?
			//total, posSplit := candidateClass.NumCandidatesWithNSplitParts(i)
			//fmt.Printf("%s #splits %v #realizations %v %v\n", candidateClass.Base3(), i, total, posSplit)
		}
	}
}

func TestExactNumCandClasses(t *testing.T) {
	for n := 1; n <= 20; n++ {
		upperBound := int(2 * math.Pow(3.0, float64(n-1)))
		if is := exactNumberOfCEC(n); is > upperBound {
			t.Errorf("n=%v: upper bound %v, got %v ", n, upperBound, is)
		}
		//fmt.Printf("%v %v\n", n, exactNumberOfCEC(n))
		//fmt.Printf("%v %v\n", n, upperBound)

	}
}

func TestCompose_PartReferenceAcrossCEC(t *testing.T) {

	es, err := types.NewElastic(os.Getenv("ES_URL_TEST"), "dev.test")
	assert.Equal(t, nil, err)

	query := "really love ever"

	classes, err := FindParts(query, es, "test")
	assert.Equal(t, nil, err)

	cecs := Compose(query, classes)

	cec := cecs[0]

	cecs[0].PECs[0].Parts = make(map[bool]map[int][]*types.Part)
	cecs[0].PECs[0].Parts[false] = make(map[int][]*types.Part)

	cecs[0].PECs[0].Parts[false][0] = []*types.Part{{Text: "test test"}}

	assert.Equal(t, "test test", cec.PECs[0].Parts[false][0][0].Text)

}

var cec []*types.CandidateEquivalenceClass

var pec []*types.PartEquivalenceClass

func benchmarkCompose(query string, b *testing.B) {

	es, err := types.NewElastic(
		os.Getenv("ES_URL_TEST"),
		"dev.1m2",
	)

	if err != nil {
		b.Fatal(err)
	}

	// get all pecs and their equivalence class
	pec, err = FindParts(query, es, "test")
	if err != nil {
		b.Fatal(err)
	}

	b.ResetTimer()

	for n := 0; n < b.N; n++ {

		cec = Compose(query, pec)

	}

	log.Debugf("#pec: %v", len(pec))
	log.Debugf("#cec: %v", len(cec))
}

func BenchmarkCompose1(b *testing.B) {
	benchmarkCompose(util.WordsToString(strings.Fields(util.BenchmarkQuery)[:1]), b)
}
func BenchmarkCompose2(b *testing.B) {
	benchmarkCompose(util.WordsToString(strings.Fields(util.BenchmarkQuery)[:2]), b)
}
func BenchmarkCompose3(b *testing.B) {
	benchmarkCompose(util.WordsToString(strings.Fields(util.BenchmarkQuery)[:3]), b)
}
func BenchmarkCompose4(b *testing.B) {
	benchmarkCompose(util.WordsToString(strings.Fields(util.BenchmarkQuery)[:4]), b)
}
func BenchmarkCompose5(b *testing.B) {
	benchmarkCompose(util.WordsToString(strings.Fields(util.BenchmarkQuery)[:5]), b)
}
func BenchmarkCompose6(b *testing.B) {
	benchmarkCompose(util.WordsToString(strings.Fields(util.BenchmarkQuery)[:6]), b)
}
func BenchmarkCompose7(b *testing.B) {
	benchmarkCompose(util.WordsToString(strings.Fields(util.BenchmarkQuery)[:7]), b)
}
func BenchmarkCompose8(b *testing.B) {
	benchmarkCompose(util.WordsToString(strings.Fields(util.BenchmarkQuery)[:8]), b)
}
func BenchmarkCompose9(b *testing.B) {
	benchmarkCompose(util.WordsToString(strings.Fields(util.BenchmarkQuery)[:9]), b)
}
func BenchmarkCompose10(b *testing.B) {
	benchmarkCompose(util.WordsToString(strings.Fields(util.BenchmarkQuery)[:10]), b)
}
func BenchmarkCompose11(b *testing.B) {
	benchmarkCompose(util.WordsToString(strings.Fields(util.BenchmarkQuery)[:11]), b)
}
func BenchmarkCompose12(b *testing.B) {
	benchmarkCompose(util.WordsToString(strings.Fields(util.BenchmarkQuery)[:12]), b)
}
func BenchmarkCompose13(b *testing.B) {
	benchmarkCompose(util.WordsToString(strings.Fields(util.BenchmarkQuery)[:13]), b)
}
func BenchmarkCompose14(b *testing.B) {
	benchmarkCompose(util.WordsToString(strings.Fields(util.BenchmarkQuery)[:14]), b)
}
func BenchmarkCompose15(b *testing.B) {
	benchmarkCompose(util.WordsToString(strings.Fields(util.BenchmarkQuery)[:15]), b)
}
func BenchmarkCompose16(b *testing.B) {
	benchmarkCompose(util.WordsToString(strings.Fields(util.BenchmarkQuery)[:16]), b)
}
func BenchmarkCompose17(b *testing.B) {
	benchmarkCompose(util.WordsToString(strings.Fields(util.BenchmarkQuery)[:17]), b)
}
func BenchmarkCompose18(b *testing.B) {
	benchmarkCompose(util.WordsToString(strings.Fields(util.BenchmarkQuery)[:18]), b)
}
func BenchmarkCompose19(b *testing.B) {
	benchmarkCompose(util.WordsToString(strings.Fields(util.BenchmarkQuery)[:19]), b)
}
func BenchmarkCompose20(b *testing.B) {
	benchmarkCompose(util.WordsToString(strings.Fields(util.BenchmarkQuery)[:20]), b)
}
