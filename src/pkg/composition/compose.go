package composition

import (
	"gitlab.ethz.ch/chgerber/MessageComposition/src/pkg/types"
	"gitlab.ethz.ch/chgerber/monitor"
	"math"
	"strconv"
	"strings"
)

/*
Combinations(K, setN):
  if k > length(setN): return "no Combinations possible"
  if k == 0: return "empty combination"
  # Combinations Including the first item:
  return (first-item-of setN) combined Combinations(K-1, all-but-first-of setN)
   union Combinations(K-1, all-but-first-of setN)
*/

// base3Range returns the range of the base-3 numbers as decimal numbers which covers all possible base-3 representations of CEC with respect to an n word input query.
func base3Range(n int) (min int, max int) {
	min = int(math.Pow(3.0, float64(n-1)))
	max = int(math.Pow(3.0, float64(n)) - 1)
	return min, max
}

// Compose returns all possible candidate equivalence classes (also incomplete ones) with respect to the phrase query.
func Compose(phraseQuery string, pec []*types.PartEquivalenceClass) (candidates []*types.CandidateEquivalenceClass) {
	defer monitor.Elapsed()()

	n := len(strings.Fields(phraseQuery))
	// compute candidate base3 representation range
	min, max := base3Range(n)

	// iterate through all candidates
	for i := min; i <= max; i++ {

		neededParts := candidateToParts(i)
		parts := partsAvailable(neededParts, pec)

		if len(neededParts) > 0 && parts != nil {

			candidates = append(candidates, &types.CandidateEquivalenceClass{ID: i, PECs: parts, TargetText: phraseQuery})
		}
	}
	return candidates
}

// candidateToParts returns the ordered (starting at MSB) partEquivalence classes the base3 candidate consists of or nil if the candidate is invalid
// e.g. valid 1200 invalid: 1222, 1202
// e.g. also the trivial candidate 2000 is removed
// TODO: Optimization: compute candidates by exact recursive formula. However not sure if it really is faster!
func candidateToParts(candidate int) (pecs []*types.PartEquivalenceClass) {

	base3Cdte := strconv.FormatInt(int64(candidate), 3)

	var partStartIdx int
	for i := 0; i < len(base3Cdte); i++ {
		digit := base3Cdte[i]

		if i == 0 {
			partStartIdx = -1
			if digit == '1' {
				partStartIdx = 0
			}
		} else {
			// remove all but one of the candidates with the same missing pecs.
			if digit == '2' && partStartIdx == -1 {
				return nil
			}
			// if new part starts
			if digit != '0' {
				// save ended part if it is a present part
				if partStartIdx >= 0 {
					//TODO: Shouldn't use PartEquivalenceClass when I don't need the text field
					part := types.PartEquivalenceClass{StartIDX: partStartIdx, EndIDX: i - 1}
					pecs = append(pecs, &part)
				}
				// if new part is a present one
				if digit == '1' {
					partStartIdx = i
				} else {
					partStartIdx = -1
				}
			}
		}

		// last digit
		if i == len(base3Cdte)-1 && digit != '2' {
			// save last part if it is a present part
			if partStartIdx >= 0 {
				part := types.PartEquivalenceClass{StartIDX: partStartIdx, EndIDX: i}
				pecs = append(pecs, &part)
			}
		}

	}

	return pecs
}

// partsAvailable tests if all part equivalence classes as specified by neededParts are available in the presentParts and returns the corresponding presentParts.
func partsAvailable(neededParts []*types.PartEquivalenceClass, presentParts []*types.PartEquivalenceClass) (parts []*types.PartEquivalenceClass) {
	// NOTE: make sure the returned pecs are ordered -> will be the case if neededPart is ordered -> neededParts is ordered
	for _, neededPart := range neededParts {
		found := false
		for i := range presentParts {
			if neededPart.StartIDX == presentParts[i].StartIDX && neededPart.EndIDX == presentParts[i].EndIDX {
				found = true
				// add splits info to candidate

				parts = append(parts, presentParts[i])
				break
			}
		}
		if !found {
			return nil
		}
	}
	return parts
}

// exactNumberOfCEC computes the exact number of possible Candidate Equivalence Classes for a phrase query of length n words.
// The resulting sequence when iterating n from 1,2,3...
// 2 5 13 34 89 233 610 1597 4181 10946 28657 75025 196418 514229 1346269 3524578 9227465 24157817 63245986 165580141, ...
// Fibonacci: 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987, 1597, 2584, 4181, 6765, 10946, 17711, 28657, 46368, 75025, 121393, 196418, 317811, ...
// The sequence happens to be a modified version of the Fibonacci sequence by starting with 2 and skipping every second number.
func exactNumberOfCEC(n int) int {
	return noConsecutiveBlocksStartingWith2(2, n)
}

func noConsecutiveBlocksStartingWith2(x int, n int) int {
	if n == 0 {
		return 1
	}
	if x == 2 {
		return noConsecutiveBlocksStartingWith2(1, n-1) + noConsecutiveBlocksStartingWith2(2, n-1)
	}
	return 2*noConsecutiveBlocksStartingWith2(1, n-1) + noConsecutiveBlocksStartingWith2(2, n-1)
}
