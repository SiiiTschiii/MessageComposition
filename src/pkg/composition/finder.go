// Package composition provides capabilities to compose all available candidate equivalence classes for a given input phrase query.
package composition

import (
	"gitlab.ethz.ch/chgerber/MessageComposition/src/pkg/types"
	"gitlab.ethz.ch/chgerber/MessageComposition/src/pkg/util"
	"gitlab.ethz.ch/chgerber/monitor"
	"strings"
)

// computePartEquivalenceClasses computes all possible substrings of text and creates returns a PartEquivalenceClass for each
// The granularity of the substring computation is space delimited words.
// e.g. "i am" has three substrings "i am", "i" and "am".
// src: https://www.geeksforgeeks.org/program-print-substrings-given-string/
func computePartEquivalenceClasses(text string) (pecs []*types.PartEquivalenceClass) {

	words := strings.Fields(text)
	n := len(words)
	// Pick starting point
	for s := 1; s <= n; s++ {
		// Pick ending point
		for e := 0; e <= n-s; e++ {
			// Print characters from current
			// starting point to current ending point.
			var part []string
			j := s + e - 1
			startIDX := e
			endIDX := j
			for k := e; k <= j; k++ {
				part = append(part, words[k])
			}
			pecs = append(pecs, &types.PartEquivalenceClass{Text: util.WordsToString(part), StartIDX: startIDX, EndIDX: endIDX})
		}
	}
	return pecs
}

// FindParts performs a phrase search for all possible substrings of the inputQuery and returns PartEquivalenceClasses for all substrings that produce at least one result along with phrase query results in form of the positional postings.
// The number of returned number of PartEquivalenceClass grows with a complexity of O(n^2). With n the number of words in the inputQuery.
func FindParts(inputQuery string, elasticIdx *types.Elastic, playlistID string) (availablePECs []*types.PartEquivalenceClass, err error) {
	defer monitor.Elapsed()()

	partEquivalenceClasses := computePartEquivalenceClasses(inputQuery)

	// fill part texts into an array to send one request -> batch processing/reduce latency
	var pecTexts []string
	for _, partEquivalenceClass := range partEquivalenceClasses {

		pecTexts = append(pecTexts, partEquivalenceClass.Text)
	}

	// perform request
	freqNoSplit, freqWithSplit, err := elasticIdx.GetNumberOfParts(playlistID, pecTexts...)
	if err != nil {
		return nil, err
	}

	for i, partEquivalenceClass := range partEquivalenceClasses {

		// if at least one result exists add this equivalence partEquivalenceClass
		if freqWithSplit[i]+freqNoSplit[i] > 0 {

			// determine split range and frequency of this part equivalence partEquivalenceClass

			partEquivalenceClass.SplitPartFreq = freqWithSplit[i]
			partEquivalenceClass.NotSplitPartFreq = freqNoSplit[i]

			availablePECs = append(availablePECs, partEquivalenceClass)
		}

	}

	return availablePECs, nil
}
