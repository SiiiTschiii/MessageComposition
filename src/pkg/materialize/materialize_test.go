package materialize

import (
	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"gitlab.ethz.ch/chgerber/MessageComposition/src/pkg/composition"
	"gitlab.ethz.ch/chgerber/MessageComposition/src/pkg/score"
	"gitlab.ethz.ch/chgerber/MessageComposition/src/pkg/types"
	"gitlab.ethz.ch/chgerber/MessageComposition/src/pkg/util"
	"gitlab.ethz.ch/chgerber/annotation/v2"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"os"
	"strings"
	"testing"
	"time"
)

func createTestAnnotations(texts ...string) (as []*annotation.Annotation) {

	for _, text := range texts {
		a := &annotation.Annotation{
			ID: primitive.NewObjectID(),
			Subtitle: annotation.Subtitle{
				Text: text,
			},
		}
		as = append(as, a)
	}

	return as
}

func TestMain(m *testing.M) {

	log.SetLevel(log.WarnLevel)

	// run all tests and exit
	os.Exit(m.Run())
}

func TestMaterialize(t *testing.T) {
	es, err := types.NewElastic(
		os.Getenv("ES_URL_TEST"),
		"dev.test",
	)

	if err != nil {
		t.Fatal(err)
	}

	// target message to compose
	phrase := "what the hell dude"

	// get all parts and their equivalence class
	classes, err := composition.FindParts(phrase, es, "test")
	if err != nil {
		t.Fatal(err)
	}

	// compose all possible candidate equivalence classes with the available part equivalence classes
	cec := composition.Compose(phrase, classes)

	referee := score.NewReferee(
		&score.NumPart{},
		&score.Similarity{
			W:            1.0,
			StringMetric: score.NewLevenshteinWord()},
		&score.BalancedPart{1},
		&score.Split{1},
	)

	_, candidates, err := RealizeTop(100, es, referee, cec, score.Popularity{W: 1, LevelThresholds: []int{1e0, 1e1}}, "test")

	assert.Equal(t, nil, err)
	assert.Equal(t, 100, len(candidates))

	//for _, candidate := range candidates {
	//
	//	log.Warnf("%s %d %f %s", candidate.Base3(), candidate.NumSplits(), candidate.Score, candidate.Text())
	//
	//}

}

func TestMaterialize_MultipleSplitRealizations(t *testing.T) {

	annos := createTestAnnotations(
		"my name is arni it really is",
		"you cannot hide you",
		"this is eth zurich",
		"happy birthday to you",
		"happy birthday to you arni",
		"happy birthday",
		"to you walt",
		"walt",
	)

	es, err := types.NewElastic(os.Getenv("ES_URL_TEST"), strings.ToLower(t.Name())+primitive.NewObjectID().Hex())
	if err != nil {
		t.Fatal(err)
	}

	defer es.DeleteIndex()

	err = es.AddAnnotations(annos)
	if err != nil {
		t.Fatal(err)
	}

	time.Sleep(2 * time.Second)

	// target message to compose
	phrase := "happy birthday to you walt"

	// get all parts and their equivalence class
	classes, err := composition.FindParts(phrase, es, "test")
	assert.Equal(t, nil, err)

	// compose all possible candidate equivalence classes with the available part equivalence classes
	cecs := composition.Compose(phrase, classes)

	for _, cec := range cecs {
		if cec.Base3() == "10100" {

			// Test realizeCEC
			candidates, err := realizeCEC(
				&types.ScoredCEC{
					CandidateEquivalenceClass: cec,
					Score:                     0.0,
					Splits:                    0,
				},
				10, es, score.Popularity{W: 1, LevelThresholds: []int{}}, "test")

			assert.Equal(t, nil, err)
			assert.Equal(t, 1, len(candidates))

			// TestNumCandidatesWithNSplitParts
			num, splitPos := cec.NumCandidatesWithNSplitParts(0)
			assert.Equal(t, 1, num)
			assert.Equal(t, 1, len(splitPos))
			assert.Equal(t, "00", splitPos[0])

			num, splitPos = cec.NumCandidatesWithNSplitParts(1)
			assert.Equal(t, 2, num)
			assert.Equal(t, 1, len(splitPos))
			assert.Equal(t, "10", splitPos[0])

			// TestRealizeTop
			referee := score.NewRefereeDefault()
			_, candidates, err = RealizeTop(10, es, referee, []*types.CandidateEquivalenceClass{cec}, score.Popularity{W: 1, LevelThresholds: []int{}}, "test")
			assert.Equal(t, nil, err)
			assert.Equal(t, 3, len(candidates))

		}
	}

}

func TestCECScoredCEC_RetrieveParts(t *testing.T) {

	es, err := types.NewElastic(os.Getenv("ES_URL_TEST"), "dev.test")
	assert.Equal(t, nil, err)

	cecZeroSplit := types.ScoredCEC{
		CandidateEquivalenceClass: &types.CandidateEquivalenceClass{
			ID: util.Base3ToBase10("1021"),
			PECs: []*types.PartEquivalenceClass{
				{
					Text:             "thank you",
					StartIDX:         0,
					EndIDX:           1,
					SplitPartFreq:    2,
					NotSplitPartFreq: 38,
				},
				{
					Text:             "is this",
					StartIDX:         0,
					EndIDX:           1,
					SplitPartFreq:    1,
					NotSplitPartFreq: 2,
				},
			},
			TargetText: "thank you is this",
		},
		Splits: 0,
	}

	realizationsBase2string, _, err := retrieveParts(&cecZeroSplit, 100, es, score.Popularity{LevelThresholds: []int{}}, "test")
	assert.Equal(t, nil, err)
	assert.Equal(t, 1, len(realizationsBase2string))
	assert.Equal(t, 120, cecZeroSplit.CandidateEquivalenceClass.NumCandidates())
	num, _ := cecZeroSplit.NumCandidatesWithNSplitParts(0)
	assert.Equal(t, 76, num)

	assert.Equal(t, 0, len(cecZeroSplit.PECs[0].Parts[true][0]))
	assert.Equal(t, 38, len(cecZeroSplit.PECs[0].Parts[false][0]))
	assert.Equal(t, 0, len(cecZeroSplit.PECs[1].Parts[true][0]))
	assert.Equal(t, 2, len(cecZeroSplit.PECs[1].Parts[false][0]))

	cecOneSplit := types.ScoredCEC{
		CandidateEquivalenceClass: &types.CandidateEquivalenceClass{
			ID: util.Base3ToBase10("1021"),
			PECs: []*types.PartEquivalenceClass{
				{
					Text:             "thank you",
					StartIDX:         0,
					EndIDX:           1,
					SplitPartFreq:    2,
					NotSplitPartFreq: 38,
				},
				{
					Text:             "is this",
					StartIDX:         0,
					EndIDX:           1,
					SplitPartFreq:    1,
					NotSplitPartFreq: 2,
				},
			},
			TargetText: "thank you is this",
		},
		Splits: 1,
	}

	realizationsBase2string, _, err = retrieveParts(&cecOneSplit, 100, es, score.Popularity{LevelThresholds: []int{}}, "test")
	assert.Equal(t, nil, err)
	assert.Equal(t, 2, len(realizationsBase2string))
	assert.Equal(t, 120, cecOneSplit.CandidateEquivalenceClass.NumCandidates())
	num, _ = cecOneSplit.NumCandidatesWithNSplitParts(1)
	assert.Equal(t, 42, num)

	assert.Equal(t, 3, len(cecOneSplit.PECs[0].Parts[true][0]))
	assert.Equal(t, 38, len(cecOneSplit.PECs[0].Parts[false][0]))
	assert.Equal(t, 1, len(cecOneSplit.PECs[1].Parts[true][0]))
	assert.Equal(t, 2, len(cecOneSplit.PECs[1].Parts[false][0]))

	cecTwoSplit := types.ScoredCEC{
		CandidateEquivalenceClass: &types.CandidateEquivalenceClass{
			ID: util.Base3ToBase10("1021"),
			PECs: []*types.PartEquivalenceClass{
				{
					Text:             "thank you",
					StartIDX:         0,
					EndIDX:           1,
					SplitPartFreq:    2,
					NotSplitPartFreq: 38,
				},
				{
					Text:             "is this",
					StartIDX:         0,
					EndIDX:           1,
					SplitPartFreq:    1,
					NotSplitPartFreq: 2,
				},
			},
			TargetText: "thank you is this",
		},
		Splits: 2,
	}

	realizationsBase2string, _, err = retrieveParts(&cecTwoSplit, 100, es, score.Popularity{LevelThresholds: []int{}}, "test")
	assert.Equal(t, nil, err)
	assert.Equal(t, 1, len(realizationsBase2string))
	assert.Equal(t, 120, cecTwoSplit.CandidateEquivalenceClass.NumCandidates())
	num, _ = cecTwoSplit.NumCandidatesWithNSplitParts(2)
	assert.Equal(t, 2, num)

	assert.Equal(t, 3, len(cecTwoSplit.PECs[0].Parts[true][0]))
	assert.Equal(t, 0, len(cecTwoSplit.PECs[0].Parts[false][0]))
	assert.Equal(t, 1, len(cecTwoSplit.PECs[1].Parts[true][0]))
	assert.Equal(t, 0, len(cecTwoSplit.PECs[1].Parts[false][0]))
}

func TestMaterialize_HasSplitPart(t *testing.T) {
	assert.Equal(t, false, hasPart(2, true, []string{"00010", "10000"}, '9', []string{"0090", "0000"}))
	assert.Equal(t, true, hasPart(2, true, []string{"00010", "10100"}, '9', []string{"0090", "0000"}))
}

func TestMaterialize_HasNonSplitPart(t *testing.T) {
	assert.Equal(t, false, hasPart(3, false, []string{"11111", "11111"}, '9', []string{"0090", "0009"}))
	assert.Equal(t, false, hasPart(3, false, []string{"11111", "11111"}, '9', []string{"0090", "0000"}))
	assert.Equal(t, true, hasPart(3, false, []string{"01011", "11101"}, '9', []string{"0090", "0009"}))
	assert.Equal(t, true, hasPart(3, false, []string{"01011", "11101"}, '9', []string{"0099", "0000"}))
}

func TestMaterialize_PopularityOnePart(t *testing.T) {

	popScore := score.Popularity{W: 1, LevelThresholds: []int{1e0, 1e1, 1e2}}

	referee := score.NewReferee(
		&score.NumPart{
			W: 0,
		},
		&score.Similarity{
			W:            0,
			StringMetric: score.NewLevenshteinWord()},
		&score.BalancedPart{
			W: 0,
		},
		&score.Split{
			W: 0,
		},
		&popScore,
	)
	num := 100

	es, err := types.NewElastic(os.Getenv("ES_URL_TEST"), "dev.test")
	assert.Equal(t, nil, err)

	query := "ever"

	// get all parts and their equivalence class
	classes, err := composition.FindParts(query, es, "test")
	assert.Equal(t, nil, err)

	// compose all possible candidate equivalence classes with the available part equivalence classes
	cecs := composition.Compose(query, classes)

	_, candidates, err = RealizeTop(num, es, referee, cecs, popScore, "test")

	assert.Equal(t, true, len(candidates) > 0)
	assert.Equal(t, true, len(candidates[0].Parts) > 0)

	// check for annotation with shared count >= 15
	for _, part := range candidates[0].Parts {
		if part.Text == "ever" {

			assert.Equal(t, "that God ever created I tell you that", part.Doc.Subtitle.Text)
			assert.Equal(t, true, len(part.Doc.Usage) > 0)
			for _, use := range part.Doc.Usage {
				if use.Text == "ever" {
					assert.Equal(t, true, use.Shared >= 15)
				}
			}
		}
	}
}

func TestMaterialize_PopularityMultiplePart(t *testing.T) {
	popScore := score.Popularity{W: 1, LevelThresholds: []int{1e0, 1e1, 1e2}}

	referee := score.NewReferee(
		&score.NumPart{
			W: 1,
		},
		&score.Similarity{
			W:            2,
			StringMetric: score.NewLevenshteinWord()},
		&score.BalancedPart{
			W: 0,
		},
		&score.Split{
			W: 1,
		},
		&popScore,
	)
	num := 100

	es, err := types.NewElastic(os.Getenv("ES_URL_TEST"), "dev.test")
	assert.Equal(t, nil, err)

	query := "really love ever"

	// get all parts and their equivalence class
	classes, err := composition.FindParts(query, es, "test")
	assert.Equal(t, nil, err)

	// compose all possible candidate equivalence classes with the available part equivalence classes
	cecs := composition.Compose(query, classes)

	_, candidates, err = RealizeTop(num, es, referee, cecs, popScore, "test")

	assert.Equal(t, true, len(candidates) > 0)
	assert.Equal(t, true, len(candidates[0].Parts) == 2)

	// check for annotation with shared count >= 15
	for _, part := range candidates[0].Parts {
		if part.Text == "really love" {

			assert.Equal(t, "stupid if you really love this country", part.Doc.Subtitle.Text)
			assert.Equal(t, true, len(part.Doc.Usage) > 0)
			for _, use := range part.Doc.Usage {
				if use.Text == "really love" {
					assert.Equal(t, true, use.Shared >= 15)
				}
			}
		}

		if part.Text == "ever" {

			assert.Equal(t, "that God ever created I tell you that", part.Doc.Subtitle.Text)
			assert.Equal(t, true, len(part.Doc.Usage) > 0)
			for _, use := range part.Doc.Usage {
				if use.Text == "ever" {
					assert.Equal(t, true, use.Shared >= 15)
				}
			}
		}
	}
}

func TestMaterialize_InsertInOrder(t *testing.T) {

	oderedList := []types.ScoredCEC{
		{Score: 1},
		{Score: 0.9},
		{Score: 0.8},
		{Score: 0.7},
		{Score: 0.6},
	}

	// append in the middle
	item := types.ScoredCEC{
		Score: 0.75,
	}

	result := insertInOrder(oderedList, item)
	assert.Equal(t, 6, len(result))
	assert.Equal(t, 0.8, result[2].Score)
	assert.Equal(t, 0.75, result[3].Score)
	assert.Equal(t, 0.7, result[4].Score)

	// append at the end
	item = types.ScoredCEC{
		Score: 0.55,
	}

	result = insertInOrder(oderedList, item)
	assert.Equal(t, 6, len(result))
	assert.Equal(t, 0.55, result[5].Score)
	assert.Equal(t, 0.6, result[4].Score)

	// append at the beginning
	item = types.ScoredCEC{
		Score: 1.1,
	}

	result = insertInOrder(oderedList, item)
	assert.Equal(t, 6, len(result))
	assert.Equal(t, 1.1, result[0].Score)
	assert.Equal(t, 1.0, result[1].Score)

}

var candidates []types.Candidate

func benchmarkMaterialize(query string, b *testing.B) {

	popScore := score.Popularity{W: 1, LevelThresholds: []int{1e0}}

	referee := score.NewReferee(
		&score.NumPart{
			W: 1,
		},
		&score.Similarity{
			W:            1,
			StringMetric: score.NewLevenshteinWord()},
		&score.BalancedPart{
			W: 1,
		},
		&score.Split{
			W: 1,
		},
		&popScore,
	)

	es, err := types.NewElastic(
		os.Getenv("ES_URL_TEST"),
		"dev.1m2",
	)

	if err != nil {
		b.Fatal(err)
	}

	num := 100

	// get all parts and their equivalence class
	classes, err := composition.FindParts(query, es, "test")
	if err != nil {
		b.Fatal(err)
	}

	log.Debugf("#pec: %v", len(classes))

	// compose all possible candidate equivalence classes with the available part equivalence classes
	cecs := composition.Compose(query, classes)

	log.Debugf("#cec: %v", len(cecs))

	b.ResetTimer()
	for n := 0; n < b.N; n++ {

		_, candidates, err = RealizeTop(num, es, referee, cecs, popScore, "test")
		if err != nil {
			b.Fatal(err)
		}
	}

	log.Debugf("#candidates: %v", len(candidates))

}

func BenchmarkMaterialize1(b *testing.B) {
	benchmarkMaterialize(util.WordsToString(strings.Fields(util.BenchmarkQuery)[:1]), b)
}
func BenchmarkMaterialize2(b *testing.B) {
	benchmarkMaterialize(util.WordsToString(strings.Fields(util.BenchmarkQuery)[:2]), b)
}
func BenchmarkMaterialize3(b *testing.B) {
	benchmarkMaterialize(util.WordsToString(strings.Fields(util.BenchmarkQuery)[:3]), b)
}
func BenchmarkMaterialize4(b *testing.B) {
	benchmarkMaterialize(util.WordsToString(strings.Fields(util.BenchmarkQuery)[:4]), b)
}
func BenchmarkMaterialize5(b *testing.B) {
	benchmarkMaterialize(util.WordsToString(strings.Fields(util.BenchmarkQuery)[:5]), b)
}
func BenchmarkMaterialize6(b *testing.B) {
	benchmarkMaterialize(util.WordsToString(strings.Fields(util.BenchmarkQuery)[:6]), b)
}
func BenchmarkMaterialize7(b *testing.B) {
	benchmarkMaterialize(util.WordsToString(strings.Fields(util.BenchmarkQuery)[:7]), b)
}
func BenchmarkMaterialize8(b *testing.B) {
	benchmarkMaterialize(util.WordsToString(strings.Fields(util.BenchmarkQuery)[:8]), b)
}
func BenchmarkMaterialize9(b *testing.B) {
	benchmarkMaterialize(util.WordsToString(strings.Fields(util.BenchmarkQuery)[:9]), b)
}
func BenchmarkMaterialize10(b *testing.B) {
	benchmarkMaterialize(util.WordsToString(strings.Fields(util.BenchmarkQuery)[:10]), b)
}
func BenchmarkMaterialize11(b *testing.B) {
	benchmarkMaterialize(util.WordsToString(strings.Fields(util.BenchmarkQuery)[:11]), b)
}
func BenchmarkMaterialize12(b *testing.B) {
	benchmarkMaterialize(util.WordsToString(strings.Fields(util.BenchmarkQuery)[:12]), b)
}
func BenchmarkMaterialize13(b *testing.B) {
	benchmarkMaterialize(util.WordsToString(strings.Fields(util.BenchmarkQuery)[:13]), b)
}
func BenchmarkMaterialize14(b *testing.B) {
	benchmarkMaterialize(util.WordsToString(strings.Fields(util.BenchmarkQuery)[:14]), b)
}
func BenchmarkMaterialize15(b *testing.B) {
	benchmarkMaterialize(util.WordsToString(strings.Fields(util.BenchmarkQuery)[:15]), b)
}
func BenchmarkMaterialize16(b *testing.B) {
	benchmarkMaterialize(util.WordsToString(strings.Fields(util.BenchmarkQuery)[:16]), b)
}
func BenchmarkMaterialize17(b *testing.B) {
	benchmarkMaterialize(util.WordsToString(strings.Fields(util.BenchmarkQuery)[:17]), b)
}
func BenchmarkMaterialize18(b *testing.B) {
	benchmarkMaterialize(util.WordsToString(strings.Fields(util.BenchmarkQuery)[:18]), b)
}
func BenchmarkMaterialize19(b *testing.B) {
	benchmarkMaterialize(util.WordsToString(strings.Fields(util.BenchmarkQuery)[:19]), b)
}
func BenchmarkMaterialize20(b *testing.B) {
	benchmarkMaterialize(util.WordsToString(strings.Fields(util.BenchmarkQuery)[:20]), b)
}
