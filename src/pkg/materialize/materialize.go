package materialize

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	"gitlab.ethz.ch/chgerber/MessageComposition/src/pkg/score"
	"gitlab.ethz.ch/chgerber/MessageComposition/src/pkg/types"
	"gitlab.ethz.ch/chgerber/MessageComposition/src/pkg/util"
	"gitlab.ethz.ch/chgerber/monitor"
	"sort"
	"strconv"
)

// retrieveParts retrieves all Parts from the db and appends them to given ScoredCEC
func retrieveParts(sCEC *types.ScoredCEC, n int, es *types.Elastic, popularityScore score.Popularity, playlistID string) (splitPositionStrings []string, popularityLevelStrings []string, err error) {

	// find possible realizations split info in base2 representation, one digit per part. 1: part with split, 0: part has no split
	_, splitPositionStrings = sCEC.NumCandidatesWithNSplitParts(sCEC.NumSplits())

	maxPopularity := len(popularityScore.LevelThresholds)

	if maxPopularity >= 1 {
		popularityLevelStrings, err = util.FindSumOfDigitNumbers(sCEC.NumParts(), sCEC.Popularity, popularityScore.Base())
		if err != nil {
			return nil, nil, err
		}
	} else {
		popularityLevelStrings = []string{fmt.Sprintf("%0*s", sCEC.NumParts(), "")}
	}

	// query for the annotations behind the parts. SplitPart when '1' in any realization string, NonSplitPart when '0'
	// TODO retrieving n instances of each pec could be less in most cases
	// TODO make PEC change persistent

	for i, p := range sCEC.PECs {

		// TODO only retrieve if not if not yet present

		if sCEC.PECs[i].Parts == nil {

			sCEC.PECs[i].Parts = make(map[bool]map[int][]*types.Part)
			sCEC.PECs[i].Parts[false] = make(map[int][]*types.Part)
			sCEC.PECs[i].Parts[true] = make(map[int][]*types.Part)

		}

		if sCEC.PECs[i].PartFrequency == nil {
			sCEC.PECs[i].PartFrequency = make(map[bool]map[int]int)
			sCEC.PECs[i].PartFrequency[false] = make(map[int]int)
			sCEC.PECs[i].PartFrequency[true] = make(map[int]int)
		}

		// TODO retrieve split/non-split of all pop levels in one batch

		// TODO make PEC a global reference accross CEC and only retrieve them once

		for popularity := 0; popularity <= maxPopularity; popularity++ {

			popularityLevelChar := strconv.FormatInt(int64(popularity), popularityScore.Base())[0]

			if hasPart(i, false, splitPositionStrings, popularityLevelChar, popularityLevelStrings) {

				// Only retrieve parts once per sCEC
				if _, ok := sCEC.PECs[i].Parts[false][popularity]; !ok {

					min, max, err := popularityScore.Range(popularity)
					if err != nil {
						return nil, nil, err
					}

					log.Debugf("%s %v %v new PEC: %s %s %v", sCEC.Base3(), sCEC.Splits, sCEC.Popularity, sCEC.PECs[i].Text, "false", popularity)
					// retrieve n NonSplitParts
					parts, totalHits, err := es.RetrieveNonSplitPartsRange(p.Text, n, min, max, playlistID)
					if err != nil {
						return nil, nil, err
					}

					sCEC.PECs[i].Parts[false][popularity] = parts
					sCEC.PECs[i].PartFrequency[false][popularity] = totalHits

				} else {
					//log.Warnf("%s %v %v NOT new PEC: %s %s %v", sCEC.Base3(), sCEC.Splits, sCEC.Popularity, sCEC.PECs[i].Text, "false", popularity)
				}
			}

			if hasPart(i, true, splitPositionStrings, popularityLevelChar, popularityLevelStrings) {

				// Only retrieve parts once per sCEC
				if _, ok := sCEC.PECs[i].Parts[true][popularity]; !ok {

					min, max, err := popularityScore.Range(popularity)
					if err != nil {
						return nil, nil, err
					}

					log.Debugf("%s %v %v new PEC: %s %s %v", sCEC.Base3(), sCEC.Splits, sCEC.Popularity, sCEC.PECs[i].Text, "true", popularity)
					// retrieve n SplitParts
					parts, totalHits, err := es.RetrieveSplitPartsRange(p.Text, n, min, max, playlistID)
					if err != nil {
						return nil, nil, err
					}

					sCEC.PECs[i].Parts[true][popularity] = parts
					sCEC.PECs[i].PartFrequency[true][popularity] = totalHits

				} else {
					//log.Warnf("%s %v %v NOT new PEC: %s %s %v", sCEC.Base3(), sCEC.Splits, sCEC.Popularity, sCEC.PECs[i].Text, "true", popularity)
				}
			}
		}
	}

	return splitPositionStrings, popularityLevelStrings, err
}

// realizeCEC realizes at most n Candidates for the given CEC
func realizeCEC(sCEC *types.ScoredCEC, n int, es *types.Elastic, popularity score.Popularity, playlistID string) (candidates []types.Candidate, err error) {

	splitPositionStrings, popularityLevelStrings, err := retrieveParts(sCEC, n, es, popularity, playlistID)
	if err != nil {
		return nil, err
	}

	for _, splitPositionString := range splitPositionStrings {

		for _, popularityPositionString := range popularityLevelStrings {

			if len(candidates) >= n {
				break
			}

			resultsThisRealization := make([]types.Candidate, 0)

			// iterate through all parts
			for partIdx, pec := range sCEC.PECs {

				candsWithPartI := make([]types.Candidate, 0)
				numToGo := n - len(candidates)

				s := string(popularityPositionString[partIdx])
				level, err := strconv.ParseInt(s, popularity.Base(), 0)
				if err != nil {
					return nil, err
				}

				// If part is missing -> stop and reset commenced candidates
				if len(pec.Parts[false][int(level)]) == 0 {

					resultsThisRealization = []types.Candidate{}
					break
				}

				var parts []*types.Part

				if splitPositionString[partIdx] == '0' {

					parts = pec.Parts[false][int(level)]
				} else {

					parts = pec.Parts[true][int(level)]
				}

				// how many iteration
				var numRep int
				if partIdx == 0 {

					numRep = 1
				} else if len(resultsThisRealization) > len(parts) {

					// p1 p2
					// A  A
					// B  B
					// C  A
					// second iteration
					// A  B
					// B  A
					// C  B
					numRep = len(parts)
				} else {

					// p1 p2
					// A  A
					// B  B
					// second iteration
					// A  C
					// B  A
					// third iteration
					// A  B
					// B  C
					numRep = len(resultsThisRealization)
				}

				for rep := 0; rep < numRep; rep++ {

					numToGo = n - len(candidates) - len(candsWithPartI)

					// create part and add it to candidates
					if numToGo == 0 {
						break

					}

					candsWithPartI = append(candsWithPartI, addPartsToNCandidates(parts, resultsThisRealization, sCEC, numToGo, rep)...)

				}

				resultsThisRealization = candsWithPartI
			}
			candidates = append(candidates, resultsThisRealization...)
		}

	}

	return candidates, nil
}

// addToNCandidates appends nextPart to at most n given candidates and returns them. If len(candidates) > n, the first n are chosen.
func addPartsToNCandidates(nextParts []*types.Part, candidates []types.Candidate, sCEC *types.ScoredCEC, n int, permOffset int) (results []types.Candidate) {

	// if nextPart = first part
	if len(candidates) == 0 {

		for _, part := range nextParts {
			results = append(results, types.Candidate{

				TargetText: sCEC.TargetText,
				Parts:      []types.Part{*part},
				Score:      sCEC.Score,
				CEC:        sCEC.CandidateEquivalenceClass,
				Popularity: sCEC.Popularity,
			})
		}

	} else {

		if len(candidates) >= len(nextParts) {

			for i, candidate := range candidates {

				if i >= n {
					break
				}

				var partIdx int

				if len(candidates) == len(nextParts) {

					partIdx = (i + permOffset) % len(nextParts)
				} else {

					partIdx = i % len(nextParts)
				}

				candidate.Parts = append(candidate.Parts, *nextParts[partIdx])
				results = append(results, candidate)
			}
		} else {

			for i := range nextParts {

				if i >= n {
					break
				}

				candidateIdx := i % len(candidates)

				candidate := candidates[candidateIdx]
				candidate.Parts = append(candidate.Parts, *nextParts[i])
				results = append(results, candidate)
			}
		}

	}

	return results
}

func updateTopScoreCandidate(sCEC types.ScoredCEC, referee score.Referee) types.ScoredCEC {

	// increase split count
	sCECsplit := sCEC
	sCECsplit.Score = 0
	if sCEC.Splits < sCEC.NumPEC() {
		sCECsplit.Splits++
		sCECsplit.Score = referee.Score(&sCECsplit)
	}

	// decrease popularity count
	sCECpop := sCEC
	sCECpop.Score = 0
	if sCEC.Popularity > 0 {
		sCECpop.Popularity--
		sCECpop.Score = referee.Score(&sCECpop)
	}

	// see which results has a higher score and make sure that zero weight scores do not count
	if sCECsplit.Score > sCECpop.Score && !util.FloatEquals(sCECsplit.Score, sCEC.Score) {
		return sCECsplit
	}

	return sCECpop
}

// RealizeTop realizes the top n (if existent) candidates that belong to the cec according to the scoring metrics defines by referee
func RealizeTop(n int, es *types.Elastic, referee score.Referee, cec []*types.CandidateEquivalenceClass, popularity score.Popularity, playlistID string) (scec []types.ScoredCEC, results []types.Candidate, err error) {
	defer monitor.Elapsed()()

	var orderedMaxCEC []types.ScoredCEC

	for i := range cec {
		orderedMaxCEC = append(orderedMaxCEC, types.ScoredCEC{CandidateEquivalenceClass: cec[i], Splits: 0, Popularity: cec[i].NumPEC() * len(popularity.LevelThresholds)})
		orderedMaxCEC[i].Score = referee.Score(&orderedMaxCEC[i])
	}

	// sort in the order of the Score
	sort.Slice(orderedMaxCEC, func(i, j int) bool {
		return orderedMaxCEC[i].Score > orderedMaxCEC[j].Score
	})

	togo := n

	for togo > 0 && len(orderedMaxCEC) > 0 {
		// find up to n candidates in the top cec
		maxCEC := orderedMaxCEC[0]

		candidates, err := realizeCEC(&maxCEC, togo, es, popularity, playlistID)
		if err != nil {
			return nil, nil, err
		}

		if n := len(candidates); n > 0 {
			scec = append(scec, maxCEC)
			results = append(results, candidates...)
			togo = togo - n
		}

		if orderedMaxCEC[0].MoreLowerScoreLevels() {

			// recompute max Score
			orderedMaxCEC[0] = updateTopScoreCandidate(orderedMaxCEC[0], referee)

			// re-sort by inserting the updated item
			orderedMaxCEC = insertInOrder(orderedMaxCEC[1:], orderedMaxCEC[0])

		} else if len(orderedMaxCEC) > 1 {

			// remove the CEC from the list
			orderedMaxCEC = orderedMaxCEC[1:]
		} else {

			// all cec in queue processed
			orderedMaxCEC = []types.ScoredCEC{}
			break
		}

	}

	return scec, results, nil

}

func insertInOrder(orderedList []types.ScoredCEC, item types.ScoredCEC) []types.ScoredCEC {
	for i := range orderedList {
		if orderedList[i].Score < item.Score {
			// make space for one additional item
			orderedList = append(orderedList, types.ScoredCEC{})
			// shift slice
			_ = copy(orderedList[i+1:], orderedList[i:])
			// insert item
			orderedList[i] = item
			return orderedList
		}
	}

	// item is smaller than list members
	return append(orderedList, item)

}

func hasPart(pos int, split bool, splitPositionStrings []string, popularityLevel uint8, popularityLevelStrings []string) bool {

	splitReq := false

	for _, s := range splitPositionStrings {
		if s[pos] == '1' && split {
			splitReq = true
			break
		}

		if s[pos] == '0' && !split {
			splitReq = true
			break
		}
	}

	if !splitReq {
		return false
	}

	for _, s := range popularityLevelStrings {
		if s[pos] == popularityLevel {
			return true
		}

	}

	return false
}
