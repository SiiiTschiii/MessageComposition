package score

import (
	"gitlab.ethz.ch/chgerber/MessageComposition/src/pkg/util"
	"testing"
)

func TestLevenshtein_Unnequal(t *testing.T) {
	var str1 = "asheville"
	var str2 = "arizona"
	ls := NewLevenshtein()
	expI := 8
	if is := ls.Distance(str1, str2); is != expI {
		t.Errorf("Expected '%v' got '%v'", expI, is)
	}
}

func TestLevenshteinSimilarity_Unnequal(t *testing.T) {
	var str1 = "asheville"
	var str2 = "arizona"
	ls := NewLevenshtein()
	expF := 1.0 / 9.0
	if is := ls.Similarity(str1, str2); !util.FloatEquals(is, expF) {
		t.Errorf("Expected '%v' got '%v'", expF, is)
	}
}

func TestLevenshtein_Equal(t *testing.T) {
	var str1 = "asheville"
	ls := NewLevenshtein()
	expI := 0
	if is := ls.Distance(str1, str1); is != expI {
		t.Errorf("Expected '%v' got '%v'", expI, is)
	}
}

func TestLevenshteinSimilarity_Equal(t *testing.T) {
	var str1 = "asheville"
	ls := NewLevenshtein()
	expF := 1.0
	if is := ls.Similarity(str1, str1); !util.FloatEquals(is, expF) {
		t.Errorf("Expected '%v' got '%v'", expF, is)
	}
}

func TestLevenshteinSimilarity_EqualEmpty(t *testing.T) {
	var str1 = ""
	var str2 = ""
	ls := NewLevenshtein()
	expF := 1.0
	if is := ls.Similarity(str1, str2); !util.FloatEquals(is, expF) {
		t.Errorf("Expected '%v' got '%v'", expF, is)
	}
}

func TestLevenshteinWord_Deletion(t *testing.T) {
	str1 := "hello donald"
	str2 := "hello"
	lsw := NewLevenshteinWord()
	expI := 1
	if is := lsw.Distance(str1, str2); is != expI {
		t.Errorf("Expected '%v' got '%v'", expI, is)
	}
}

func TestLevenshteinWord_DeletionEmpty(t *testing.T) {
	str1 := "hello donald"
	str2 := ""
	lsw := NewLevenshteinWord()
	expI := 2
	if is := lsw.Distance(str1, str2); is != expI {
		t.Errorf("Expected '%v' got '%v'", expI, is)
	}
}

func TestLevenshteinSimilarityWord_DeletionEmpty(t *testing.T) {
	str1 := "hello donald"
	str2 := ""
	lsw := NewLevenshteinWord()
	expF := 0.0
	if is := lsw.Similarity(str1, str2); !util.FloatEquals(is, expF) {
		t.Errorf("Expected '%v' got '%v'", expF, is)
	}
}

func TestLevenshteinWord_Insertion(t *testing.T) {
	str1 := "hello "
	str2 := "hello hoi arni now ethz"
	lsw := NewLevenshteinWord()
	expI := 4
	if is := lsw.Distance(str1, str2); is != expI {
		t.Errorf("Expected '%v' got '%v'", expI, is)
	}
}

func TestLevenshteinSimilarityWord_Insertion(t *testing.T) {
	str1 := "hello "
	str2 := "hello hoi arni now ethz"
	lsw := NewLevenshteinWord()
	expF := 0.2
	if is := lsw.Similarity(str1, str2); !util.FloatEquals(is, expF) {
		t.Errorf("Expected '%v' got '%v'", expF, is)
	}
}

func TestLevenshteinWord_Substitution(t *testing.T) {
	str1 := "hello donald duck"
	str2 := "hello arni best"
	lsw := NewLevenshteinWord()
	expI := 2
	if is := lsw.Distance(str1, str2); is != expI {
		t.Errorf("Expected '%v' got '%v'", expI, is)
	}
}

func TestLevenshteinSimilarityWord_Substitution(t *testing.T) {
	str1 := "hello donald duck"
	str2 := "hello arni best"
	lsw := NewLevenshteinWord()
	expF := 1.0 / 3.0
	if is := lsw.Similarity(str1, str2); !util.FloatEquals(is, expF) {
		t.Errorf("Expected '%v' got '%v'", expF, is)
	}
}

func TestLevenshteinSimilarityWord_EqualEmpty(t *testing.T) {
	str1 := ""
	str2 := ""
	lsw := NewLevenshteinWord()
	expF := 1.0
	if is := lsw.Similarity(str1, str2); !util.FloatEquals(is, expF) {
		t.Errorf("Expected '%v' got '%v'", expF, is)
	}
}

func TestLevenshteinSimilarityWord_Equal(t *testing.T) {
	str1 := "hello arni donald"
	str2 := "hello arni donald"
	lsw := NewLevenshteinWord()
	expF := 1.0
	if is := lsw.Similarity(str1, str2); !util.FloatEquals(is, expF) {
		t.Errorf("Expected '%v' got '%v'", expF, is)
	}
}

func TestLevenshteinSimilarity_WordRuneContrast(t *testing.T) {
	str1 := "hello arni donal"
	str2 := "hello arni donald"
	lsw := NewLevenshteinWord()
	expF := 2.0 / 3.0 // 0.666
	if is := lsw.Similarity(str1, str2); !util.FloatEquals(is, expF) {
		t.Errorf("Expected '%v' got '%v'", expF, is)
	}

	ls := NewLevenshtein()
	expF = 1.0 - 1.0/17.0 // 0.9412
	if is := ls.Similarity(str1, str2); !util.FloatEquals(is, expF) {
		t.Errorf("Expected '%v' got '%v'", expF, is)
	}
}
