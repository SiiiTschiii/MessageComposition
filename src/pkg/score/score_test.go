package score

import (
	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"gitlab.ethz.ch/chgerber/MessageComposition/src/pkg/types"
	"gitlab.ethz.ch/chgerber/MessageComposition/src/pkg/util"
	"gitlab.ethz.ch/chgerber/annotation/v2"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"os"
	"testing"
)

func TestMain(m *testing.M) {

	log.SetLevel(log.WarnLevel)

	os.Exit(m.Run())
}

func createTestAnnotations(texts ...string) (as []*annotation.Annotation) {

	for _, text := range texts {
		a := &annotation.Annotation{
			ID: primitive.NewObjectID(),
			Subtitle: annotation.Subtitle{
				Text: text,
			},
		}
		as = append(as, a)
	}

	return as
}

func TestScoreNumParts_2Parts3Words(t *testing.T) {

	c := types.Candidate{
		TargetText: "this is arni",
		Parts: []types.Part{
			{
				Text:  "is arni",
				Split: true,
				Doc: &annotation.Annotation{
					Subtitle: annotation.Subtitle{
						Text: "my name is arni",
					},
				},
			},
			{
				Text:  "this",
				Split: true,
				Doc: &annotation.Annotation{
					Subtitle: annotation.Subtitle{
						Text: "this is eth zurich",
					},
				},
			},
		},
	}

	expF := 0.5
	score := (&NumPart{1.0}).Apply(&c)
	//score := numPartScore(len(strings.Fields(c.TargetText)), len(c.Parts))
	if expF != score {
		t.Errorf("Expected '%e' got '%e'", expF, score)
	}

}

func TestScoreNumParts_1Part3Words(t *testing.T) {

	c := types.Candidate{
		TargetText: "this is arni",
		Parts: []types.Part{
			{
				Text:  "this is arni",
				Split: true,
				Doc: &annotation.Annotation{
					Subtitle: annotation.Subtitle{
						Text: "my name this is arni",
					},
				},
			},
		},
	}

	expF := 1.0
	score := (&NumPart{1.0}).Apply(&c)
	if expF != score {
		t.Errorf("Expected '%e' got '%e'", expF, score)
	}
}

func TestScoreNumParts_3Parts3Words(t *testing.T) {

	c := types.Candidate{
		TargetText: "this is arni",
		Parts: []types.Part{
			{
				Text:  "this",
				Split: true,
				Doc: &annotation.Annotation{
					Subtitle: annotation.Subtitle{
						Text: "my name this hello",
					},
				},
			},
			{
				Text:  "is",
				Split: true,
				Doc: &annotation.Annotation{
					Subtitle: annotation.Subtitle{
						Text: "this is eth zurich",
					},
				},
			},
			{
				Text:  "arni",
				Split: false,
				Doc: &annotation.Annotation{
					Subtitle: annotation.Subtitle{
						Text: "arni",
					},
				},
			},
		},
	}

	expF := 0.0
	score := (&NumPart{1.0}).Apply(&c)
	if expF != score {
		t.Errorf("Expected '%e' got '%e'", expF, score)
	}
}

func TestScoreNumParts_0Parts(t *testing.T) {

	c := types.Candidate{
		TargetText: "this is arni",
	}

	score := (&NumPart{1}).Apply(&c)
	expF := 0.0
	if expF != score {
		t.Errorf("Expected '%e' as the candidate has zero parts, got '%e'", expF, score)
	}
}

func TestScoreNumParts_morePartsThanWords(t *testing.T) {

	c := types.Candidate{
		TargetText: "this is arni",
		Parts: []types.Part{
			{
				Text:  "this",
				Split: true,
				Doc: &annotation.Annotation{
					Subtitle: annotation.Subtitle{
						Text: "my name this hello",
					},
				},
			},
			{
				Text:  "is",
				Split: true,
				Doc: &annotation.Annotation{
					Subtitle: annotation.Subtitle{
						Text: "this is eth zurich",
					},
				},
			},
			{
				Text:  "ar",
				Split: true,
				Doc: &annotation.Annotation{
					Subtitle: annotation.Subtitle{
						Text: "ar the",
					},
				},
			},
			{
				Text:  "ni",
				Split: true,
				Doc: &annotation.Annotation{
					Subtitle: annotation.Subtitle{
						Text: "hey hei ni",
					},
				},
			},
		},
	}

	score := (&NumPart{1.0}).Apply(&c)
	expF := 0.0
	if expF != score {
		t.Errorf("Expected '%e' as the candidate has more parts than the query has words, got '%e'", expF, score)
	}
}

func TestScoreBalancedParts_AllEqualLengthOne(t *testing.T) {

	c := types.Candidate{
		TargetText: "i love this party it’s just unreal",
		Parts: []types.Part{
			{
				Text:  "i",
				Split: true,
				Doc: &annotation.Annotation{
					Subtitle: annotation.Subtitle{
						Text: "cnmalksd i sdkjfn",
					},
				},
			},
			{
				Text:  "love",
				Split: true,
				Doc: &annotation.Annotation{
					Subtitle: annotation.Subtitle{
						Text: "chgdscg love memkdcsc",
					},
				},
			},
			{
				Text:  "this",
				Split: true,
				Doc: &annotation.Annotation{
					Subtitle: annotation.Subtitle{
						Text: "dfdsaf this cndsakjc",
					},
				},
			},
			{
				Text:  "party",
				Split: true,
				Doc: &annotation.Annotation{
					Subtitle: annotation.Subtitle{
						Text: "dfdsaf party cndsakjc",
					},
				},
			},
			{
				Text:  "it’s",
				Split: true,
				Doc: &annotation.Annotation{
					Subtitle: annotation.Subtitle{
						Text: "dfdsaf it's cndsakjc",
					},
				},
			},
			{
				Text:  "just",
				Split: true,
				Doc: &annotation.Annotation{
					Subtitle: annotation.Subtitle{
						Text: "dfdsaf just cndsakjc",
					},
				},
			},
			{
				Text:  "unreal",
				Split: true,
				Doc: &annotation.Annotation{
					Subtitle: annotation.Subtitle{
						Text: "dfdsaf unreal cndsakjc",
					},
				},
			},
		},
	}

	expF := 1.0
	score := (&BalancedPart{1.0}).Apply(&c)
	if expF != score {
		t.Errorf("Expected '%e' got '%e'", expF, score)
	}
}

func TestScoreBalancedParts_OnePart(t *testing.T) {
	c := types.Candidate{
		TargetText: "i love this party it’s just unreal",
		Parts: []types.Part{
			{
				Text:  "i love this party it’s just unreal",
				Split: true,
				Doc: &annotation.Annotation{
					Subtitle: annotation.Subtitle{
						Text: "jkdskf i love this party it's just unreal jflsafhs",
					},
				},
			},
		},
	}

	expF := 1.0
	score := (&BalancedPart{1.0}).Apply(&c)
	if expF != score {
		t.Errorf("Expected '%e' got '%e'", expF, score)
	}
}

func TestScoreBalancedParts_VeryUnbalanced(t *testing.T) {

	c := types.Candidate{
		TargetText: "i love this party it’s just unreal",
		Parts: []types.Part{
			{
				Text:  "i love this party it's just",
				Split: true,
				Doc: &annotation.Annotation{
					Subtitle: annotation.Subtitle{
						Text: "cnmalksd i love this party it's just sdkjfn",
					},
				},
			},
			{
				Text:  "unreal",
				Split: true,
				Doc: &annotation.Annotation{
					Subtitle: annotation.Subtitle{
						Text: "chgdscg unreal memkdcsc",
					},
				},
			},
		},
	}

	expF := 1.0 / 3.5
	score := (&BalancedPart{1.0}).Apply(&c)
	if expF != score {
		t.Errorf("Expected '%e' got '%e'", expF, score)
	}
}

func TestScoreBalancedParts_EmptyTarget(t *testing.T) {

	c := types.Candidate{
		TargetText: "",
		Parts: []types.Part{
			{
				Text:  "i love this party it's just",
				Split: true,
				Doc: &annotation.Annotation{
					Subtitle: annotation.Subtitle{
						Text: "cnmalksd i love this party it's just sdkjfn",
					},
				},
			},
			{
				Text:  "unreal",
				Split: true,
				Doc: &annotation.Annotation{
					Subtitle: annotation.Subtitle{
						Text: "chgdscg unreal memkdcsc",
					},
				},
			},
		},
	}

	score := (&BalancedPart{1.0}).Apply(&c)
	expF := (2.0 / 7.0) * 1.0
	if !util.FloatEquals(expF, score) {
		t.Errorf("Expected '%e' as candidate target is the empty string, got '%e'", expF, score)
	}

}

func TestScoreBalancedParts_EmptyPart(t *testing.T) {

	c := types.Candidate{
		TargetText: "i love this party it’s just unreal",
		Parts: []types.Part{
			{
				Text:  "i love this party it's just",
				Split: true,
				Doc: &annotation.Annotation{
					Subtitle: annotation.Subtitle{
						Text: "cnmalksd i love this party it's just sdkjfn",
					},
				},
			},
			{
				Text:  "",
				Split: true,
				Doc: &annotation.Annotation{
					Subtitle: annotation.Subtitle{
						Text: "chgdscg unreal memkdcsc",
					},
				},
			},
		},
	}

	score := (&BalancedPart{1.0}).Apply(&c)

	expF := 0.0
	if expF != score {
		t.Errorf("Expected '%e' as candidate has a part with an empty string, got '%e'", expF, score)
	}

}

func TestScoreSimilarity(t *testing.T) {

	c := types.Candidate{
		TargetText: "i love this party it's just unreal stuff",
		Parts: []types.Part{
			{
				Text:  "i love this party it's just",
				Split: true,
				Doc: &annotation.Annotation{
					Subtitle: annotation.Subtitle{
						Text: "cnmalksd i love this party it's just sdkjfn",
					},
				},
			},
			{
				Text:  "unreal",
				Split: true,
				Doc: &annotation.Annotation{
					Subtitle: annotation.Subtitle{
						Text: "chgdscg unreal memkdcsc",
					},
				},
			},
		},
	}

	expF := 7.0 / 8.0
	score := (&Similarity{1.0, NewLevenshteinWord()}).Apply(&c)
	if expF != score {
		t.Errorf("Expected '%e' got '%e'", expF, score)
	}

	expF = 0.85
	score = (&Similarity{1.0, NewLevenshtein()}).Apply(&c)

	if expF != score {
		t.Errorf("Expected '%e' got '%e'", expF, score)
	}
}

func TestScoreSplit_TwoSplitsTwoParts(t *testing.T) {

	c := types.Candidate{
		TargetText: "this is arni",
		Parts: []types.Part{
			{
				Text:  "is arni",
				Split: true,
				Doc: &annotation.Annotation{
					Subtitle: annotation.Subtitle{
						Text: "my name is arni",
					},
				},
			},
			{
				Text:  "this",
				Split: true,
				Doc: &annotation.Annotation{
					Subtitle: annotation.Subtitle{
						Text: "this has eth zurich",
					},
				},
			},
		},
	}

	expF := 0.0
	score := (&Split{1.0}).Apply(&c)
	if expF != score {
		t.Errorf("Expected '%e' got '%e'", expF, score)
	}
}

func TestScoreSplit_NoSplit(t *testing.T) {

	c := types.Candidate{
		TargetText: "this is arni",
		Parts: []types.Part{
			{
				Text:  "is arni",
				Split: false,
				Doc: &annotation.Annotation{
					Subtitle: annotation.Subtitle{
						Text: "is arni",
					},
				},
			},
			{
				Text:  "this",
				Split: false,
				Doc: &annotation.Annotation{
					Subtitle: annotation.Subtitle{
						Text: "this",
					},
				},
			},
		},
	}

	expF := 1.0
	score := (&Split{1.0}).Apply(&c)
	if expF != score {
		t.Errorf("Expected '%e' got '%e'", expF, score)
	}
}

func TestScoreSplit_1Split4Parts(t *testing.T) {

	c := types.Candidate{
		TargetText: "this here is what he arni",
		Parts: []types.Part{
			{
				Text:  "this",
				Split: false,
				Doc: &annotation.Annotation{
					Subtitle: annotation.Subtitle{
						Text: "this",
					},
				},
			},
			{
				Text:  "here",
				Split: true,
				Doc: &annotation.Annotation{
					Subtitle: annotation.Subtitle{
						Text: "fjsd here dsfkdsjf",
					},
				},
			},
			{
				Text:  "is what he",
				Split: false,
				Doc: &annotation.Annotation{
					Subtitle: annotation.Subtitle{
						Text: "is what he",
					},
				},
			},
			{
				Text:  "arni",
				Split: false,
				Doc: &annotation.Annotation{
					Subtitle: annotation.Subtitle{
						Text: "arni",
					},
				},
			},
		},
	}

	expF := 3.0 / 4.0
	score := (&Split{1.0}).Apply(&c)
	if expF != score {
		t.Errorf("Expected '%e' got '%e'", expF, score)
	}
}

func TestAggregatedScore_MoreBalancedPreferred(t *testing.T) {

	c1 := types.Candidate{
		TargetText: "this is arni here",
		Parts: []types.Part{
			{
				Text:  "this is arni",
				Split: true,
				Doc: &annotation.Annotation{
					Subtitle: annotation.Subtitle{
						Text: "this is arni here",
					},
				},
			},
			{
				Text:  "here",
				Split: true,
				Doc: &annotation.Annotation{
					Subtitle: annotation.Subtitle{
						Text: "ethz this is arni zurich here",
					},
				},
			},
		},
	}

	c2 := types.Candidate{
		TargetText: "this is arni here",
		Parts: []types.Part{
			{
				Text:  "this is",
				Split: true,
				Doc: &annotation.Annotation{
					Subtitle: annotation.Subtitle{
						Text: "this is eth zurich",
					},
				},
			},
			{
				Text:  "arni here",
				Split: true,
				Doc: &annotation.Annotation{
					Subtitle: annotation.Subtitle{
						Text: "my name is arni here",
					},
				},
			},
		},
	}

	referee := NewRefereeDefault()
	score1 := referee.Score(&c1)
	score2 := referee.Score(&c2)

	if score2 < score1 {
		t.Errorf("Expected Score of candidate 2 '%e' to be higher than Score of candidate 1 '%e'", score2, score1)
	}
}

func TestAggregatedScore_LessPartsPreferred(t *testing.T) {

	c1 := types.Candidate{
		TargetText: "this is arni here",
		Parts: []types.Part{
			{
				Text:  "arni here",
				Split: true,
				Doc: &annotation.Annotation{
					Subtitle: annotation.Subtitle{
						Text: "my name is arni here",
					},
				},
			},
			{
				Text:  "this is",
				Split: true,
				Doc: &annotation.Annotation{
					Subtitle: annotation.Subtitle{
						Text: "this is eth zurich",
					},
				},
			},
		},
	}

	c2 := types.Candidate{
		TargetText: "this is arni here",
		Parts: []types.Part{
			{
				Text:  "this is arni here",
				Split: true,
				Doc: &annotation.Annotation{
					Subtitle: annotation.Subtitle{
						Text: "my name this is arni here",
					},
				},
			},
		},
	}

	referee := NewRefereeDefault()
	score1 := referee.Score(&c1)
	score2 := referee.Score(&c2)

	if score2 < score1 {
		t.Errorf("Expected Score of candidate 2 '%e' to be higher than Score of candidate 1 '%e'", score2, score1)
	}
}

func TestAggregatedScore_NoSplitPreferred(t *testing.T) {

	c1 := types.Candidate{
		TargetText: "this is arni",
		Parts: []types.Part{
			{
				Text:  "this is arni",
				Split: true,
				Doc: &annotation.Annotation{
					Subtitle: annotation.Subtitle{
						Text: "my name this is arni",
					},
				},
			},
		},
	}

	c2 := types.Candidate{
		TargetText: "this is arni",
		Parts: []types.Part{
			{
				Text:  "this is arni",
				Split: false,
				Doc: &annotation.Annotation{
					Subtitle: annotation.Subtitle{
						Text: "this is arni",
					},
				},
			},
		},
	}

	referee := NewRefereeDefault()
	score1 := referee.Score(&c1)
	score2 := referee.Score(&c2)

	if score2 < score1 {
		t.Errorf("Expected Score of candidate 2 '%e' to be higher than Score of candidate 1 '%e'", score2, score1)
	}
}

func TestAggregatedScore_SimilarPreferred(t *testing.T) {

	c1 := types.Candidate{
		TargetText: "this is arni",
		Parts: []types.Part{
			{
				Text:  "this is arn",
				Split: false,
				Doc: &annotation.Annotation{
					Subtitle: annotation.Subtitle{
						Text: "this is arni",
					},
				},
			},
		},
	}

	c2 := types.Candidate{
		TargetText: "this is arni",
		Parts: []types.Part{
			{
				Text:  "this is arni",
				Split: false,
				Doc: &annotation.Annotation{
					Subtitle: annotation.Subtitle{
						Text: "this is arni",
					},
				},
			},
		},
	}

	referee := NewRefereeDefault()
	score1 := referee.Score(&c1)
	score2 := referee.Score(&c2)

	if score2 < score1 {
		t.Errorf("Expected Score of candidate 2 '%e' to be higher than Score of candidate 1 '%e'", score2, score1)
	}
}

func TestAggregatedScore_SimilarEqualOneSplit(t *testing.T) {

	c1 := types.Candidate{
		TargetText: "this is arni",
		Parts: []types.Part{
			{
				Text:  "this is",
				Split: false,
				Doc: &annotation.Annotation{
					Subtitle: annotation.Subtitle{
						Text: "this is",
					},
				},
			},
		},
	}

	c2 := types.Candidate{
		TargetText: "this is arni",
		Parts: []types.Part{
			{
				Text:  "this is arni",
				Split: false,
				Doc: &annotation.Annotation{
					Subtitle: annotation.Subtitle{
						Text: "this is arni",
					},
				},
			},
		},
	}

	referee := NewRefereeDefault()
	score1 := referee.Score(&c1)
	score2 := referee.Score(&c2)

	if score2 == score1 {
		t.Errorf("Expected Score of candidate 2 '%e' to be equal Score of candidate 1 '%e'", score2, score1)
	}
}

func TestPopularityScoreMax(t *testing.T) {
	p := Popularity{W: 1, LevelThresholds: []int{1e0, 1e1, 1e2, 1e3, 1e4}}
	ref := NewReferee(&p)

	c := types.Candidate{
		Parts:      []types.Part{{}, {}, {}},
		Popularity: 15,
	}
	assert.Equal(t, 1.0, p.Apply(&c))
	assert.Equal(t, true, util.FloatEquals(1.0, ref.Score(&c)))
}

func TestPopularityScoreAverage(t *testing.T) {
	p := Popularity{W: 1, LevelThresholds: []int{1e0, 1e1, 1e2, 1e3, 1e4}}
	ref := NewReferee(&p)

	c := types.Candidate{
		Parts:      []types.Part{{}, {}, {}},
		Popularity: 8,
	}

	assert.Equal(t, true, util.FloatEquals(0.5333333333333333, p.Apply(&c)))
	assert.Equal(t, true, util.FloatEquals(0.5333333333333333, ref.Score(&c)))
}

func TestPopularityScoreMin(t *testing.T) {
	p := Popularity{W: 1, LevelThresholds: []int{1e0, 1e1, 1e2, 1e3, 1e4}}
	ref := NewReferee(&p)

	c := types.Candidate{
		Parts:      []types.Part{{}, {}, {}},
		Popularity: 0,
	}

	assert.Equal(t, 0.0, p.Apply(&c))
	assert.Equal(t, true, util.FloatEquals(0.0, ref.Score(&c)))
}

func TestPopularityScoreTwoLevels(t *testing.T) {
	p := Popularity{W: 1, LevelThresholds: []int{1e0}}
	ref := NewReferee(&p)

	c := types.Candidate{
		Parts:      []types.Part{{}},
		Popularity: 1,
	}

	assert.Equal(t, true, util.FloatEquals(1.0, p.Apply(&c)))
	assert.Equal(t, true, util.FloatEquals(1.0, ref.Score(&c)))
}

func TestPopularityScoreZeroLevels(t *testing.T) {
	p := Popularity{W: 1, LevelThresholds: []int{}}
	ref := NewReferee(&p)

	c := types.Candidate{
		Parts:      []types.Part{{}, {}, {}},
		Popularity: 0,
	}

	assert.Equal(t, true, util.FloatEquals(1.0, p.Apply(&c)))
	assert.Equal(t, true, util.FloatEquals(1.0, ref.Score(&c)))
}
