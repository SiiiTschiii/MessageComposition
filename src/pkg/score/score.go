// Package score provides scoring functions to evaluate the quality of candidates.
package score

import (
	"fmt"
	"math"
	"reflect"
	"strings"
)

var defaultWeight = 1.0

// Referee provides the capabilities to Score candidates and to find the candidate equivalence classes that account for the n best candidates.
type Referee interface {
	Score(s Scorable) float64
	Functions() []Metric
}

// Scorable needs to be implemented to apply the scoring
type Scorable interface {
	// Target Text of the candidate
	Target() string
	// Number of Parts the Candidate consists of
	NumParts() int
	// Lengths of the parts. In order of occurence
	PartLengths() []int
	// Space delimited concatenation of the part texts
	Text() string
	// Number of Split Parts in the Candidate
	NumSplits() int

	// Sum of the Popularity levels of the parts
	PopularityLevel() int
}

type referee struct {
	scoringFunctions []Metric
}

// Metric is the scoring function interface to be implemented by all scoring functions.
type Metric interface {
	// Name returns the scoring metric name
	Name() string
	Apply(scorable Scorable) float64
	// Weight returns the weight of the scoring metric
	Weight() float64
	// Info returns additional information/description about the scoring metric
	Info() string
}

// NewReferee returns a referee initialized with the given scoring weights.
func NewReferee(metric ...Metric) Referee {
	return &referee{
		scoringFunctions: metric,
	}
}

// NewRefereeDefault return a referee with the default configuration of all scoring functions.
// The weights are uniformly
func NewRefereeDefault() Referee {
	return NewReferee(
		&NumPart{
			W: defaultWeight,
		},
		&Similarity{
			W:            defaultWeight,
			StringMetric: NewLevenshteinWord()},
		&BalancedPart{
			W: defaultWeight,
		},
		&Split{
			W: defaultWeight,
		},
	)
}

// Score computes the candidate Score for given candidate and returns it. Not normalized.
func (r *referee) Score(s Scorable) float64 {

	aggrScore := 0.0
	sumOfWeights := 0.0

	for _, metric := range r.scoringFunctions {
		aggrScore += metric.Apply(s)
		sumOfWeights += metric.Weight()
	}

	// normalize total score to [0.0, 1.0]
	return aggrScore / float64(sumOfWeights)
}

// Functions returns all Scoring Metrics of the referee
func (r *referee) Functions() []Metric {
	return r.scoringFunctions
}

// NumPart gives a higher Score to candidates with few parts. Max Score if candidate has one part.
// It punishes candidates that need more sources (parts) to reproduce the phrase query.
// Co-domain [0, 1]
type NumPart struct {
	W float64
}

// Weight returns the weight of the scoring metric
func (s *NumPart) Weight() float64 {
	return s.W
}

// Name returns the scoring metric name
func (s *NumPart) Name() string {
	return strings.TrimPrefix(reflect.TypeOf(s).String(), "*score.")
}

// Apply applies the scoring metric on the scorable interface and returns the weighted score.
func (s *NumPart) Apply(scorable Scorable) float64 {
	numWordsTarget := len(strings.Fields(scorable.Target()))

	score := 0.0

	if scorable.NumParts() == 0 {

		score = 0.0
	} else if scorable.NumParts() > numWordsTarget {

		score = 0.0
	} else if numWordsTarget == 1 {

		score = 1.0
	} else {

		score = 1.0 - float64(scorable.NumParts()-1)/float64(numWordsTarget-1)
	}

	return s.Weight() * score
}

// Info returns additional information/description about the scoring metric
func (s *NumPart) Info() string {
	return ""
}

// BalancedPart gives candidates that consist of multiple parts a higher Score if all its parts are of similar length.
// Co-domain [0, 1]
type BalancedPart struct {
	W float64
}

// Weight returns the weight of the scoring metric
func (s *BalancedPart) Weight() float64 {
	return s.W
}

// Name returns the scoring metric name
func (s *BalancedPart) Name() string {
	return strings.TrimPrefix(reflect.TypeOf(s).String(), "*score.")
}

// Apply applies the scoring metric on the scorable interface and returns the weighted score.
func (s *BalancedPart) Apply(scorable Scorable) float64 {
	numWordsCandidateText := 0
	numWordsParts := scorable.PartLengths()

	for _, i := range numWordsParts {
		numWordsCandidateText += i
	}

	if numWordsCandidateText == 0 {
		return 0.0
	}
	if len(numWordsParts) == 0 {
		return 0.0
	}
	numWordsAvg := float64(numWordsCandidateText) / float64(len(numWordsParts))
	min := math.MaxFloat64
	for _, partLength := range numWordsParts {
		ratio := float64(partLength) / numWordsAvg
		if ratio < min {
			min = ratio
		}
	}

	return s.Weight() * min
}

// Info returns additional information/description about the scoring metric
func (s *BalancedPart) Info() string {
	return ""
}

// Similarity computes the similarityScore Score of the candidateText with respect to it's TargetText based on the given StringMetric.
// It is a measure of completeness of a candidate.
// Co-domain [0, 1]
type Similarity struct {
	W float64
	StringMetric
}

// Weight returns the weight of the scoring metric
func (s *Similarity) Weight() float64 {
	return s.W
}

// Name returns the scoring metric name
func (s *Similarity) Name() string {
	return strings.TrimPrefix(reflect.TypeOf(s).String(), "*score.")
}

// Apply applies the scoring metric on the scorable interface and returns the weighted score.
// TODO optimization: similarityScore reduced to problem of how many words are missing. Don't have to do string comparison as the findPart only delivers matching parts
func (s *Similarity) Apply(scorable Scorable) float64 {
	return s.Weight() * s.Similarity(scorable.Target(), scorable.Text())
}

// Info returns the name of the similarity metric used
func (s *Similarity) Info() string {
	return strings.TrimPrefix(reflect.TypeOf(s.StringMetric).String(), "*score.")
}

// Split gives a higher Score to candidates with fewer split parts.
// It is a measure of how feasible it is to cut the video.
// Co-domain [0, 1]
type Split struct {
	W float64
}

// Weight returns the weight of the scoring metric
func (s *Split) Weight() float64 {
	return s.W
}

// Name returns the scoring metric name
func (s *Split) Name() string {
	return strings.TrimPrefix(reflect.TypeOf(s).String(), "*score.")
}

// Apply applies the scoring metric on the scorable interface and returns the weighted score.
// TODO In case multiparts are welcome change it so that TwoPartOneSplit is not better than OnePartOneSplit
// TODO take into account two splitScore are needed if part text is in the middle
func (s *Split) Apply(scorable Scorable) float64 {

	return s.Weight() * float64(scorable.NumParts()-scorable.NumSplits()) / float64(scorable.NumParts())
}

// Info returns additional information/description about the scoring metric
func (s *Split) Info() string {
	return ""
}

// Popularity gives a higher score to candidates that consist of parts that were often occured in candidates that were shared by users.
type Popularity struct {
	W               float64
	LevelThresholds []int
}

// Weight returns the weight of the scoring metric
func (s *Popularity) Weight() float64 {
	return s.W
}

// Name returns the scoring metric name
func (s *Popularity) Name() string {
	return strings.TrimPrefix(reflect.TypeOf(s).String(), "*score.")
}

// Apply applies the scoring metric on the scorable interface and returns the weighted score.
func (s *Popularity) Apply(scorable Scorable) float64 {
	if len(s.LevelThresholds) == 0 {
		return 1.0
	}

	score := s.Weight() * float64((1/float64(len(s.LevelThresholds)*scorable.NumParts()))*float64(scorable.PopularityLevel()))
	return score
}

// Info returns the popularity level thresholds
func (s *Popularity) Info() string {
	return fmt.Sprintf("Thresholds: %v", s.LevelThresholds)
}

// Range returns the shared counter range that corresponds to the given popularity level
func (s *Popularity) Range(level int) (min int64, max int64, err error) {

	//level out of range
	if level > len(s.LevelThresholds) {
		return 0, 0, fmt.Errorf("level %d > number of popularity levels %d", level, len(s.LevelThresholds))
	}

	// zero levels
	if len(s.LevelThresholds) == 0 {
		return int64(0), math.MaxInt64, nil
	}

	//min level
	if level == 0 {
		return int64(0), int64(s.LevelThresholds[0]), nil
	}

	// max level
	if level == len(s.LevelThresholds) {
		return int64(s.LevelThresholds[len(s.LevelThresholds)-1]), math.MaxInt64, nil
	}

	return int64(s.LevelThresholds[level-1]), int64(s.LevelThresholds[level]), nil
}

// Base returns the basis of the numbers in the popularityLevelStrings
//  2 <= base
func (s *Popularity) Base() int {

	if s.LevelThresholds == nil {

		return 2
	}

	if len(s.LevelThresholds) == 0 {

		return 2

	}
	return len(s.LevelThresholds) + 1

}
