package score

import "strings"

// StringMetric is an interface that implements the distance and similarity function for string metrics.
type StringMetric interface {
	Distance(str1, str2 string) int
	Similarity(str1, str2 string) float64
}

type levenshtein struct {
}

// NewLevenshtein returns a StringMetric interface for the levenshtein metric on a charactre level granularity.
func NewLevenshtein() StringMetric {
	return &levenshtein{}
}

// Similarity computes the levenshtein similarity of str1 with respect to str2.
// The strings are compared on character granularity.
func (ls *levenshtein) Similarity(str1, str2 string) float64 {
	// trivial case of both strings being empty. prevents division by zero
	if len(str1) == 0 && len(str2) == 0 {
		return 1.0
	}
	levDis := ls.Distance(str1, str2)
	return 1.0 - float64(levDis)/float64(max(len(str1), len(str2)))
}

// Distance computes the levenshtein distance of str1 with respect to str2.
// The strings are compared on character granularity.
func (ls *levenshtein) Distance(s1, s2 string) int {
	var str1 = []rune(s1)
	var str2 = []rune(s2)
	//src: http://www.golangprograms.com/golang-program-for-implementation-of-levenshtein-distance.html
	s1len := len(str1)
	s2len := len(str2)
	column := make([]int, len(str1)+1)

	for y := 1; y <= s1len; y++ {
		column[y] = y
	}
	for x := 1; x <= s2len; x++ {
		column[0] = x
		lastKey := x - 1
		for y := 1; y <= s1len; y++ {
			oldkey := column[y]
			var incr int
			if str1[y-1] != str2[x-1] {
				incr = 1
			}

			column[y] = minimum(column[y]+1, column[y-1]+1, lastKey+incr)
			lastKey = oldkey
		}
	}
	return column[s1len]
}

type levenshteinWord struct {
}

// NewLevenshteinWord returns a StringMetric interface for the levenshtein string metric on a word level granularity.
func NewLevenshteinWord() StringMetric {
	return &levenshteinWord{}
}

// Similarity computes the levenshtein similarity of str1 with respect to str2.
// The strings are compared on word (space delimited) granularity.
func (lfw *levenshteinWord) Similarity(s1, s2 string) float64 {
	str1 := strings.Fields(s1)
	str2 := strings.Fields(s2)
	// trivial case of both strings being empty. prevents division by zero
	if len(str1) == 0 && len(str2) == 0 {
		return 1.0
	}
	levDis := lfw.Distance(s1, s2)
	return 1.0 - float64(levDis)/float64(max(len(str1), len(str2)))
}

// Distance computes the levenshtein distance of str1 with respect to str2.
// The strings are compared on word (space delimited) granularity.
func (lfw *levenshteinWord) Distance(s1, s2 string) int {
	//adapted from: http://www.golangprograms.com/golang-program-for-implementation-of-levenshtein-distance.html
	str1 := strings.Fields(s1)
	str2 := strings.Fields(s2)
	s1len := len(str1)
	s2len := len(str2)
	column := make([]int, len(str1)+1)

	for y := 1; y <= s1len; y++ {
		column[y] = y
	}
	for x := 1; x <= s2len; x++ {
		column[0] = x
		lastKey := x - 1
		for y := 1; y <= s1len; y++ {
			oldkey := column[y]
			var incr int
			if str1[y-1] != str2[x-1] {
				incr = 1
			}

			column[y] = minimum(column[y]+1, column[y-1]+1, lastKey+incr)
			lastKey = oldkey
		}
	}
	return column[s1len]
}

// minimum returns the minimum of a,b,c.
func minimum(a, b, c int) int {
	// src: http://www.golangprograms.com/golang-program-for-implementation-of-levenshtein-distance.html
	if a < b {
		if a < c {
			return a
		}
	} else {
		if b < c {
			return b
		}
	}
	return c
}

// max returns the max of a and b.
func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}
