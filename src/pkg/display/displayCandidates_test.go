package display

import (
	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"gitlab.ethz.ch/chgerber/MessageComposition/src/pkg/composition"
	"gitlab.ethz.ch/chgerber/MessageComposition/src/pkg/materialize"
	"gitlab.ethz.ch/chgerber/MessageComposition/src/pkg/score"
	"gitlab.ethz.ch/chgerber/MessageComposition/src/pkg/types"
	"os"
	"testing"
)

func TestMain(m *testing.M) {

	log.SetLevel(log.WarnLevel)

	// run all tests and exit
	os.Exit(m.Run())
}

func TestDisplayCandidates(t *testing.T) {

	es, err := types.NewElastic(
		os.Getenv("ES_URL_TEST"),
		"dev.test",
	)

	if err != nil {
		t.Fatal(err)
	}

	// target message to compose
	phrase := "thank you this is"

	// get all parts and their equivalence class
	classes, err := composition.FindParts(phrase, es, "test")
	if err != nil {
		t.Fatal(err)
	}

	// compose all possible candidate equivalence classes with the available part equivalence classes
	cecs := composition.Compose(phrase, classes)

	referee := score.NewRefereeDefault()
	n := 100

	_, candidates, err := materialize.RealizeTop(n, es, referee, cecs, score.Popularity{W: 1, LevelThresholds: []int{1e0, 1e1}}, "test")
	assert.Equal(t, nil, err)

	err = CandidatesInHTML(candidates, "/tmp/index.html", referee)
	if err != nil {
		t.Error(err)
	}

}
