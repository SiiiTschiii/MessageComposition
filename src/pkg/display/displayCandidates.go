package display

import (
	"fmt"
	"gitlab.ethz.ch/chgerber/MessageComposition/src/pkg/score"
	"gitlab.ethz.ch/chgerber/MessageComposition/src/pkg/types"
	"gitlab.ethz.ch/chgerber/MessageComposition/src/pkg/util"
	"gitlab.ethz.ch/chgerber/video"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"time"
)

var head = `<!DOCTYPE html>
<html>
<body>`

var tail = `</body>
</html>`

// PlayVideo plays the video at absolute path file
func PlayVideo(file string) error {
	// display the results in browser
	_, err := os.Stat(filepath.Clean(file))
	if err != nil {
		return err
	}
	args := []string{filepath.Clean(file)}
	cmd := exec.Command("ffplay", args...)
	err = cmd.Start()
	if err != nil {
		return err
	}
	return nil
}

// CandidatesInBrowser displays the given candidates as HTML opened with a browser.
func CandidatesInBrowser(candidates []types.Candidate, referee score.Referee) error {
	// write results to html file for displaying
	err := CandidatesInHTML(candidates, "/tmp/candidates.html", referee)
	if err != nil {
		return err
	}

	// display the results in browser
	cmd := exec.Command("google-chrome", "/tmp/candidates.html")
	err = cmd.Start()
	if err != nil {
		return err
	}
	return nil

}

// CandidatesInHTML produces an HTML at location file which contains the given candidates.
func CandidatesInHTML(candidates []types.Candidate, file string, referee score.Referee) error {
	// head of html
	_, err := os.Create(file)
	if err != nil {
		return err
	}

	// html heading
	err = util.AppendStringToFile(file, head)
	if err != nil {
		return err
	}

	// Print scoring weights
	for _, s := range referee.Functions() {

		line := fmt.Sprintf("%s Weight: %.3f, %s<br>", s.Name(), s.Weight(), s.Info())
		err = util.AppendStringToFile(file, line)
		if err != nil {
			return err
		}
	}

	// print each candidate
	for i, candidate := range candidates {

		// print number
		line := fmt.Sprintf("%v<br>", i+1)
		err = util.AppendStringToFile(file, line)
		if err != nil {
			return err
		}

		// print score
		line = fmt.Sprintf("%.3f<br>", candidate.Score)
		err = util.AppendStringToFile(file, line)
		if err != nil {
			return err
		}

		// print target text
		line = fmt.Sprintf("%s<br>", candidate.TargetText)
		err = util.AppendStringToFile(file, line)
		if err != nil {
			return err
		}

		// print candidates
		for _, part := range candidate.Parts {
			line = partAsHTMLString(part)
			// html body: add one line
			err = util.AppendStringToFile(file, line)
			if err != nil {
				return err
			}
		}

		// add newline after candidate
		line = fmt.Sprint("<br>")
		err = util.AppendStringToFile(file, line)
		if err != nil {
			return err
		}
	}
	//tail of html
	err = util.AppendStringToFile(file, tail)
	if err != nil {
		return err
	}
	return nil
}

// partAsHTMLString turns a part into its HTML one liner representation
func partAsHTMLString(pd types.Part) string {

	media := pd.Doc.Src
	docID := pd.Doc.ID.Hex()
	startTime := video.TimeFormatFFMPEG(time.Duration(pd.StartTime()) * time.Millisecond)
	endTime := video.TimeFormatFFMPEG(time.Duration(pd.EndTime()) * time.Millisecond)

	var highlighted string
	if pd.Split {
		highlighted = pd.Highlight
	} else {
		highlighted = "<em>" + pd.ExtractHighlight() + "</em>"
	}

	highlighted = strings.ReplaceAll(highlighted, "<em>", "<span style=\"background-color: #FFFF00\"><u>")
	highlighted = strings.ReplaceAll(highlighted, "</em>", "</u></span>")

	var sharedCount int
	for _, use := range pd.Doc.Usage {
		if use.Text == pd.Text {
			sharedCount = use.Shared
		}
	}

	// append one result as one line to the html file
	line := fmt.Sprintf("<b>%s</b> | %v | %s | docID: %v | %s - %s <br>", highlighted, sharedCount, media, docID, startTime, endTime)
	return line
}
