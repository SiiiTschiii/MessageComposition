package display

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	"gitlab.ethz.ch/chgerber/MessageComposition/src/pkg/score"
	"gitlab.ethz.ch/chgerber/MessageComposition/src/pkg/types"
	"gitlab.ethz.ch/chgerber/MessageComposition/src/pkg/util"
	"os"
	"os/exec"
)

// PlotCECofBestNCandidates plots the given CEC and opens the plot in the chrome browser
// Uses a python3.6 script for plotting
func PlotCECofBestNCandidates(cecs []types.ScoredCEC, phrase string, referee score.Referee, popBase int) error {
	plotFile := "/tmp/PlotCECofBestNCandidates.csv"
	_, err := os.Create(plotFile)
	if err != nil {
		return err
	}

	// print phrase
	queryName := fmt.Sprintf("%s", phrase)
	err = util.AppendStringToFile(plotFile, queryName)
	if err != nil {
		return err
	}

	// Print scoring weights
	for _, s := range referee.Functions() {

		line := fmt.Sprintf("%s Weight: %.3f, %s", s.Name(), s.Weight(), s.Info())
		err = util.AppendStringToFile(plotFile, line)
		if err != nil {
			return err
		}
	}

	// print headers
	colName := fmt.Sprintf("%s,%s,%s,%s,%s", "score", "base3", "splits", "popularity", "freq")
	err = util.AppendStringToFile(plotFile, colName)
	if err != nil {
		return err
	}

	for i, cec := range cecs {

		numCandidates, err := cec.NumCandidates(popBase)
		if err != nil {
			return err
		}

		log.WithFields(log.Fields{
			"rank":       i + 1,
			"score":      fmt.Sprintf("%.2f", cec.Score),
			"cec":        cec.Base3(),
			"numSplits":  cec.Splits,
			"popularity": cec.Popularity,
			"freq":       numCandidates,
			"query":      phrase,
		}).Info()
		line := fmt.Sprintf("%f,\"c%s\",%v,%v,%v", cec.Score, cec.Base3(), cec.Splits, cec.Popularity, numCandidates)
		err = util.AppendStringToFile(plotFile, line)
		if err != nil {
			return err
		}
	}
	cmd := exec.Command("/usr/bin/python3.6", "plot/PlotCECofBestNCandidates.py")
	out, err := cmd.CombinedOutput()
	if err != nil {
		log.Error(string(out))
		return err
	}

	// display the results in browser
	cmd = exec.Command("google-chrome", "/tmp/PlotCECofBestNCandidates.pdf")

	err = cmd.Start()
	if err != nil {
		return err
	}

	return nil
}
