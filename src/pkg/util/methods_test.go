package util

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestBase3ToBase10(t *testing.T) {
	assert.Equal(t, 34, Base3ToBase10("1021"))
	assert.Equal(t, 57, Base3ToBase10("2010"))
}

func TestMaterialize_FindSumOfDigitNumbers(t *testing.T) {
	numbers, err := FindSumOfDigitNumbers(4, 2, 2)
	assert.Equal(t, nil, err)
	assert.Equal(t, len(BinaryNumbersWithKones(2, 4)), len(numbers))

	numbers, err = FindSumOfDigitNumbers(4, 3, 3)
	assert.Equal(t, nil, err)
	assert.Equal(t, 16, len(numbers))
}

func TestSumOfDigits(t *testing.T) {
	result, err := sumString("ffff", 16)
	assert.Equal(t, nil, err)
	assert.Equal(t, 60, result)
}
