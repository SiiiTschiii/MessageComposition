package util

import (
	"fmt"
	"math/big"
	"os"
	"strconv"
	"strings"
)

// WordsToString concatenates the words delimited by a space.
func WordsToString(words []string) string {
	var concat string
	for _, word := range words {
		concat += word + " "
	}
	return strings.TrimSuffix(concat, " ")
}

// Range defines a numerical range from Min to Max
type Range struct {
	Min int
	Max int
}

// PreProcess normalizes the given string.
// The following punctuation marks are removed: .,!?:
// The speaker change signs (hyphen headed or tailed by a space) of the .srt subtitle format is removed: " -" or " -"
// The text is made lower case
func PreProcess(text string) string {
	// replace all " -" which indicate newlines or talker changes
	text = strings.Replace(text, " -", "", -1)
	text = strings.Replace(text, "- ", "", -1)
	// remove trailing punctuation
	text = strings.Replace(text, ".", "", -1)
	text = strings.Replace(text, ",", "", -1)
	text = strings.Replace(text, "!", "", -1)
	text = strings.Replace(text, "?", "", -1)
	text = strings.Replace(text, ":", "", -1)
	// make all lowercase
	text = strings.ToLower(text)
	return text
}

// EPSILON is used as deviation tolerance of floating point number comparison.
var EPSILON = 0.00000001

// FloatEquals compares the two floating point numbers.
// It returns true if they deviate less than EPSILON from each other.
func FloatEquals(a, b float64) bool {
	if (a-b) < EPSILON && (b-a) < EPSILON {
		return true
	}
	return false
}

// AppendStringToFile appends text to the file at path.
func AppendStringToFile(path, text string) error {
	f, err := os.OpenFile(path, os.O_APPEND|os.O_WRONLY, 0600)
	if err != nil {
		return err
	}
	defer f.Close()

	text += "\n"
	_, err = f.WriteString(text)
	if err != nil {
		return err
	}
	return nil
}

// AddPrefix adds prefix in front of all strs.
func AddPrefix(prefix string, strs []string) []string {
	for i := 0; i < len(strs); i++ {
		strs[i] = prefix + strs[i]
	}
	return strs
}

// ZeroString returns a string with n '0'.
func ZeroString(n int) string {
	s := make([]rune, n, n)
	for i := 0; i < n; i++ {
		s[i] = '0'
	}
	return string(s)
}

// Exponentiate returns base^exp
func Exponentiate(base int, exp int) (result int) {

	if exp == 0 {
		return 1
	}

	if exp == 1 {
		return base
	}

	result = 1
	for i := 1; i <= exp; i++ {
		result *= base
	}
	return result
}

// Base3ToBase10 return the decimal number (base 10) of base3 number
func Base3ToBase10(base3 string) (dec int) {

	for i, digit := range base3 {
		exponent := (len(base3) - 1) - i
		if digit == '1' {
			dec += 1 * Exponentiate(3, exponent)
		} else if digit == '2' {
			dec += 2 * Exponentiate(3, exponent)
		}
	}

	return dec
}

// BinaryNumbersWithKones computes all Combinations of binary numbers of length digits with exactly k bits set to 1
func BinaryNumbersWithKones(k int, digits int) []string {
	// no Combinations possible
	if k > digits {
		return []string{}
	}
	// empty combination
	if k == 0 {
		return []string{ZeroString(digits)}
	}
	return append(AddPrefix("0", BinaryNumbersWithKones(k, digits-1)), AddPrefix("1", BinaryNumbersWithKones(k-1, digits-1))...)
}

// FindSumOfDigitNumbers returns all numbers with the given base with the given number of digits that have the given digit sum
//
// Example:
//
// Input:
// digits: 4
// sum: 2
// base: 2
//
// Returns:
// [0011 0101 0110 1001 1010 1100]
func FindSumOfDigitNumbers(digits int, sum int, base int) (numbers []string, err error) {

	for val := 0; val < Exponentiate(base, digits); val++ {
		s := strconv.FormatInt(int64(val), base)

		s = fmt.Sprintf("%0*s", digits, s)

		sumIs, err := sumString(s, base)
		if err != nil {
			return nil, err
		}

		if sumIs == sum {
			numbers = append(numbers, s)
		}
	}

	return numbers, nil
}

// src: https://rosettacode.org/wiki/Sum_digits_of_an_integer#Go
func sumString(n string, base int) (int, error) {
	i, ok := new(big.Int).SetString(n, base)
	if !ok {
		return 0, strconv.ErrSyntax
	}
	if i.Sign() < 0 {
		return 0, strconv.ErrRange
	}
	if i.BitLen() <= 64 {
		return sum(i.Uint64(), base), nil
	}
	return sumBig(i, base), nil
}

func sum(i uint64, base int) (sum int) {
	b64 := uint64(base)
	for ; i > 0; i /= b64 {
		sum += int(i % b64)
	}
	return
}

func sumBig(n *big.Int, base int) (sum int) {
	i := new(big.Int).Set(n)
	b := new(big.Int).SetUint64(uint64(base))
	r := new(big.Int)
	for i.BitLen() > 0 {
		i.DivMod(i, b, r)
		sum += int(r.Uint64())
	}
	return
}
