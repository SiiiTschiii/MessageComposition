### pprof
```bash
# run benchmark for 10s with cpu profile
go test -bench=BenchmarkPprofEndToEnd -benchtime 10s -run=^$ . -cpuprofile profile.out
# run test with memory profile
go test -run=TestPprofMessageComposition . -memprofile memprofile.out
# or add the following to goland go tool argument TODO not yet working
-bench=BenchmarkPprofEndToEnd -run=^$ . -cpuprofile profile.out

# new try
-test.run=XXX -test.bench=. -test.benchmem -test.cpuprofile=cpu.prof -test.memprofile=mem.prof

# run pprof tool on a mem profile
go tool pprof performance.test memprofile.out

# run pprof tool on a cpu profile
go tool pprof performance.test memprofile.out

# graph in browser
web

# functions with max cpu/mem usage
top10

# ordered by calling/waiting functions
top5 -cum

# show source code of a function with disassembly when clicked on
weblist Compose
```
