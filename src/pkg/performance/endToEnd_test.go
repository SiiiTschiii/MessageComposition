package performance

import (
	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"gitlab.ethz.ch/chgerber/MessageComposition/src/pkg/composition"
	"gitlab.ethz.ch/chgerber/MessageComposition/src/pkg/materialize"
	"gitlab.ethz.ch/chgerber/MessageComposition/src/pkg/score"
	"gitlab.ethz.ch/chgerber/MessageComposition/src/pkg/types"
	"gitlab.ethz.ch/chgerber/MessageComposition/src/pkg/util"
	"os"
	"strings"
	"testing"
)

var es *types.Elastic
var pecs []*types.PartEquivalenceClass
var cecs []*types.CandidateEquivalenceClass
var candidates []types.Candidate

func TestMain(m *testing.M) {

	log.SetLevel(log.WarnLevel)

	var err error
	es, err = types.NewElastic(
		os.Getenv("ES_URL_TEST"),
		"dev.test",
	)

	if err != nil {
		log.Fatal(err)
	}

	// run all tests and exit
	os.Exit(m.Run())
}

func TestPprofMessageComposition(t *testing.T) {

	phrase := util.WordsToString(strings.Fields(util.BenchmarkQuery)[:7])
	n := 100

	popScore := score.Popularity{W: 1, LevelThresholds: []int{}}

	referee := score.NewReferee(
		&score.NumPart{
			W: 1,
		},
		&score.Similarity{
			W:            1,
			StringMetric: score.NewLevenshteinWord()},
		&score.BalancedPart{
			W: 1,
		},
		&score.Split{
			W: 1,
		},
		&popScore,
	)

	// get all parts and their equivalence class
	pecs, err := composition.FindParts(phrase, es, "test")

	// compose all possible candidate equivalence classes with the available part equivalence classes
	cecs := composition.Compose(phrase, pecs)

	_, _, err = materialize.RealizeTop(n, es, referee, cecs, popScore, "test")
	assert.Equal(t, nil, err)

}

//noinspection GoUnusedParameter
func benchmarkEndToEnd(phrase string, b *testing.B) {

	popScore := score.Popularity{W: 1, LevelThresholds: []int{1e0}}

	referee := score.NewReferee(
		&score.NumPart{
			W: 1,
		},
		&score.Similarity{
			W:            1,
			StringMetric: score.NewLevenshteinWord()},
		&score.BalancedPart{
			W: 1,
		},
		&score.Split{
			W: 1,
		},
		&popScore,
	)

	num := 100
	var err error

	b.ResetTimer()

	for n := 0; n < b.N; n++ {

		// get all parts and their equivalence class
		pecs, err = composition.FindParts(phrase, es, "test")
		assert.Equal(b, nil, err)

		// compose all possible candidate equivalence classes with the available part equivalence classes
		cecs = composition.Compose(phrase, pecs)

		_, candidates, err = materialize.RealizeTop(num, es, referee, cecs, popScore, "test")
		assert.Equal(b, nil, err)

	}

	log.Debugf("#pec: %v", len(pecs))
	log.Debugf("#cec: %v", len(cecs))
	log.Debugf("#candidates: %v", len(candidates))
}

func BenchmarkEndToEnd1(b *testing.B) {
	benchmarkEndToEnd(util.WordsToString(strings.Fields(util.BenchmarkQuery)[:1]), b)
}
func BenchmarkEndToEnd2(b *testing.B) {
	benchmarkEndToEnd(util.WordsToString(strings.Fields(util.BenchmarkQuery)[:2]), b)
}
func BenchmarkEndToEnd3(b *testing.B) {
	benchmarkEndToEnd(util.WordsToString(strings.Fields(util.BenchmarkQuery)[:3]), b)
}
func BenchmarkEndToEnd4(b *testing.B) {
	benchmarkEndToEnd(util.WordsToString(strings.Fields(util.BenchmarkQuery)[:4]), b)
}
func BenchmarkEndToEnd5(b *testing.B) {
	benchmarkEndToEnd(util.WordsToString(strings.Fields(util.BenchmarkQuery)[:5]), b)
}
func BenchmarkEndToEnd6(b *testing.B) {
	benchmarkEndToEnd(util.WordsToString(strings.Fields(util.BenchmarkQuery)[:6]), b)
}
func BenchmarkEndToEnd7(b *testing.B) {
	benchmarkEndToEnd(util.WordsToString(strings.Fields(util.BenchmarkQuery)[:7]), b)
}
func BenchmarkEndToEnd8(b *testing.B) {
	benchmarkEndToEnd(util.WordsToString(strings.Fields(util.BenchmarkQuery)[:8]), b)
}
func BenchmarkEndToEnd9(b *testing.B) {
	benchmarkEndToEnd(util.WordsToString(strings.Fields(util.BenchmarkQuery)[:9]), b)
}
func BenchmarkEndToEnd10(b *testing.B) {
	benchmarkEndToEnd(util.WordsToString(strings.Fields(util.BenchmarkQuery)[:10]), b)
}
func BenchmarkEndToEnd11(b *testing.B) {
	benchmarkEndToEnd(util.WordsToString(strings.Fields(util.BenchmarkQuery)[:11]), b)
}
func BenchmarkEndToEnd12(b *testing.B) {
	benchmarkEndToEnd(util.WordsToString(strings.Fields(util.BenchmarkQuery)[:12]), b)
}
func BenchmarkEndToEnd13(b *testing.B) {
	benchmarkEndToEnd(util.WordsToString(strings.Fields(util.BenchmarkQuery)[:13]), b)
}
func BenchmarkEndToEnd14(b *testing.B) {
	benchmarkEndToEnd(util.WordsToString(strings.Fields(util.BenchmarkQuery)[:14]), b)
}
func BenchmarkEndToEnd15(b *testing.B) {
	benchmarkEndToEnd(util.WordsToString(strings.Fields(util.BenchmarkQuery)[:15]), b)
}
func BenchmarkEndToEnd16(b *testing.B) {
	benchmarkEndToEnd(util.WordsToString(strings.Fields(util.BenchmarkQuery)[:16]), b)
}
func BenchmarkEndToEnd17(b *testing.B) {
	benchmarkEndToEnd(util.WordsToString(strings.Fields(util.BenchmarkQuery)[:17]), b)
}
func BenchmarkEndToEnd18(b *testing.B) {
	benchmarkEndToEnd(util.WordsToString(strings.Fields(util.BenchmarkQuery)[:18]), b)
}
func BenchmarkEndToEnd19(b *testing.B) {
	benchmarkEndToEnd(util.WordsToString(strings.Fields(util.BenchmarkQuery)[:19]), b)
}
func BenchmarkEndToEnd20(b *testing.B) {
	benchmarkEndToEnd(util.WordsToString(strings.Fields(util.BenchmarkQuery)[:20]), b)
}
