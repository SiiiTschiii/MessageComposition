package types

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/olivere/elastic"
	log "github.com/sirupsen/logrus"
	"gitlab.ethz.ch/chgerber/annotation/v2"
	"gitlab.ethz.ch/chgerber/elastic/go/elastic6"
	"gitlab.ethz.ch/chgerber/monitor"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"math"
	"strings"
)

// Elastic is an ElasticSearch Index
type Elastic struct {
	client *elastic.Client
	Index  string
}

// NewElastic creates a new Elastic connected to the Index Index at domain url
func NewElastic(url string, index string) (*Elastic, error) {

	var client *elastic.Client
	var err error

	if strings.Contains(url, "es.amazonaws.com") {
		// Create aws client
		client, err = elastic6.NewElasticAWSClient(url)
		if err != nil {
			return nil, err
		}

	} else {
		client, err = elastic6.NewElasticClient(url)
		if err != nil {
			return nil, err
		}
	}

	exists, err := client.IndexExists(index).Do(context.Background())
	if err != nil {
		return nil, err
	}

	if !exists {
		log.Debugf("Index %s does not exist", index)
		log.Debugf("Create Index %s", index)
		err := elastic6.CreateAnnotationIndex(index, client)

		if err != nil {
			return nil, err
		}
	}

	return &Elastic{
		client: client,
		Index:  index,
	}, nil
}

// GetNumberOfParts queries the Index for documents with subtitles that contain the partText.
// Uses the Elastic MultiSearch API to allow for batch prosessing of multiple partTexts
// The returned array have the same order and length as the passed partTexts
func (esi *Elastic) GetNumberOfParts(playlistID string, partTexts ...string) (nonSplit []int, split []int, err error) {
	defer monitor.Elapsed()()

	var requests []*elastic.SearchRequest

	// create MultiSearch Query
	for _, partText := range partTexts {

		// even idx are NonSplit
		requests = append(requests, getNumberOfNonSplitParts(partText, playlistID))

		// odd idx are Split
		requests = append(requests, getNumberOfSplitParts(partText, playlistID))

	}

	// perform request
	response, err := esi.client.MultiSearch().Index(esi.Index).Add(requests...).Do(context.Background())
	if err != nil {
		return nil, nil, nil
	}

	for i, result := range response.Responses {

		// even idx are NonSplit responses
		if i%2 == 0 {
			nonSplit = append(nonSplit, int(result.TotalHits()))
		} else {
			split = append(split, int(result.TotalHits()))

		}
	}

	return nonSplit, split, err
}

func numberOfMatches(query elastic.Query) *elastic.SearchRequest {

	return elastic.NewSearchRequest().
		Query(query).
		From(0).Size(0)

}

func (esi *Elastic) retrieveDocs(query elastic.Query, n int, highlight *elastic.Highlight) (*elastic.SearchResult, error) {
	defer monitor.Elapsed()()

	result, err := esi.client.Search().
		Index(esi.Index).
		Query(query).
		Highlight(highlight).
		From(0).Size(n).
		Do(context.Background())

	if err != nil {
		return nil, err
	}

	return result, nil
}

func unmarshalToAnnotation(hit *elastic.SearchHit) (a *annotation.Annotation, err error) {
	bytes, err := hit.Source.MarshalJSON()
	if err != nil {
		return nil, err
	}

	err = json.Unmarshal(bytes, &a)
	if err != nil {
		return nil, err
	}

	// TODO fix this "_id" vs "id" mongodb/elastic issue
	id, err := primitive.ObjectIDFromHex(hit.Id)
	if err != nil {
		return nil, err
	}
	a.ID = id

	return a, nil
}

// RetrieveSplitPartsRange retrieves and returns the n SplitParts with the highest shared counter value in the range [minShared, maxShared]
func (esi *Elastic) RetrieveSplitPartsRange(targetPhrase string, n int, minShared int64, maxShared int64, playlistID string) (parts []*Part, TotalHits int, err error) {

	query := splitPartQuery(targetPhrase).
		Must(sharedCountRangeAndBoost(targetPhrase, minShared, maxShared), elastic.NewTermQuery("playlist.id", playlistID))

	result, err := esi.retrieveDocs(query, n, elastic.NewHighlight().Field("subtitle.text"))
	if err != nil {
		return nil, 0, err
	}

	for _, hit := range result.Hits.Hits {

		a, err := unmarshalToAnnotation(hit)
		if err != nil {
			return nil, 0, err
		}

		part := Part{

			Text:      targetPhrase,
			Doc:       a,
			Split:     true,
			Highlight: hit.Highlight["subtitle.text"][0],
		}
		parts = append(parts, &part)
	}

	return parts, int(result.TotalHits()), nil
}

// RetrieveSplitParts retrieves at most n SplitParts from the elastic index
func (esi *Elastic) RetrieveSplitParts(targetPhrase string, n int, playlistID string) (parts []*Part, totalHits int, err error) {
	return esi.RetrieveSplitPartsRange(targetPhrase, n, int64(0), math.MaxInt64, playlistID)
}

// RetrieveNonSplitPartsRange retrieves and returns the n NonSplitParts with the highest shared counter value in the range [minShared, maxShared]
func (esi *Elastic) RetrieveNonSplitPartsRange(targetPhrase string, n int, minShared int64, maxShared int64, playlistID string) (parts []*Part, totalHits int, err error) {

	query := nonSplitPartQuery(targetPhrase).
		Must(sharedCountRangeAndBoost(targetPhrase, minShared, maxShared),
			elastic.NewTermQuery("playlist.id", playlistID))

	result, err := esi.retrieveDocs(query, n, elastic.NewHighlight().Field("subtitle.text"))
	if err != nil {
		return nil, 0, err
	}

	for _, hit := range result.Hits.Hits {

		a, err := unmarshalToAnnotation(hit)
		if err != nil {
			return nil, 0, err
		}
		_, ok := hit.Highlight["subtitle.text"]
		if !ok {
			return parts, 0, errors.New("retrieved document doesn't contain highlight field")
		}
		if len(hit.Highlight["subtitle.text"]) == 0 {
			return parts, 0, errors.New("retrieved document has an empty highlight array")
		}

		part := Part{

			Text:      targetPhrase,
			Doc:       a,
			Split:     false,
			Highlight: hit.Highlight["subtitle.text"][0],
		}
		parts = append(parts, &part)
	}

	return parts, int(result.TotalHits()), nil
}

// RetrieveNonSplitParts retrieves at most n NonSplitParts from the elastic index
func (esi *Elastic) RetrieveNonSplitParts(targetPhrase string, n int, playlistID string) (parts []*Part, totalHits int, err error) {

	return esi.RetrieveNonSplitPartsRange(targetPhrase, n, int64(0), math.MaxInt64, playlistID)
}

// RetrievePartByID retrieves a part with the given id which is also a match for the targetPhrase from the elastic index.
func (esi *Elastic) RetrievePartByID(id string, targetPhrase string) (part Part, err error) {

	// Try nonSplitPart first
	query := elastic.NewBoolQuery().
		Must(
			elastic.NewTermQuery("_id", id),
			nonSplitPartQuery(targetPhrase),
		)

	result, err := esi.retrieveDocs(query, 1, elastic.NewHighlight().Field("subtitle.text"))
	if err != nil {
		return part, err
	}

	partSplit := false

	if len(result.Hits.Hits) != 1 {

		// Try splitPart
		query = elastic.NewBoolQuery().
			Must(
				elastic.NewTermQuery("_id", id),
				splitPartQuery(targetPhrase),
			)

		result, err = esi.retrieveDocs(query, 1, elastic.NewHighlight().Field("subtitle.text"))
		if err != nil {
			return part, err
		}

		if len(result.Hits.Hits) != 1 {
			return part, fmt.Errorf("query produced %v hits. expected 1 hit", len(result.Hits.Hits))
		}

		partSplit = true
	}

	hit := result.Hits.Hits[0]

	a, err := unmarshalToAnnotation(hit)
	if err != nil {
		return part, err
	}

	// check if highlight text is present
	_, ok := hit.Highlight["subtitle.text"]
	if !ok {
		return part, errors.New("retrieved document doesn't contain highlight field")
	}
	if len(hit.Highlight["subtitle.text"]) == 0 {
		return part, errors.New("retrieved document has an empty highlight array")
	}

	part = Part{

		Text:      targetPhrase,
		Split:     partSplit,
		Doc:       a,
		Highlight: hit.Highlight["subtitle.text"][0],
	}

	return part, nil
}

func splitPartQuery(partText string) *elastic.BoolQuery {
	numberOfWords := len(strings.Fields(partText))

	return elastic.NewBoolQuery().
		Filter(
			elastic.NewMatchPhraseQuery("subtitle.text", partText),
			elastic.NewRangeQuery("subtitle.text.words_count").Gt(numberOfWords)).
		MustNot(elastic.NewExistsQuery("subtitle.segments"))

}

func nonSplitPartQuery(partText string) *elastic.BoolQuery {
	numberOfWords := len(strings.Fields(partText))

	return elastic.NewBoolQuery().
		Filter(
			elastic.NewMatchPhraseQuery("subtitle.text", partText),
			elastic.NewBoolQuery().
				Should(
					elastic.NewTermQuery("subtitle.text.words_count", numberOfWords),
					elastic.NewExistsQuery("subtitle.segments")))
}

func sharedCountRangeAndBoost(partText string, min int64, max int64) *elastic.BoolQuery {
	numberOfWords := len(strings.Fields(partText))

	if min == int64(0) {
		return elastic.NewBoolQuery().
			Should(
				elastic.NewNestedQuery(
					"usage",
					elastic.NewFunctionScoreQuery().
						ScoreMode("first").
						BoostMode("sum").
						AddScoreFunc(
							elastic.NewFieldValueFactorFunction().
								Field("usage.shared").
								Factor(1).
								Modifier("none").
								Missing(0)).
						Query(
							elastic.NewBoolQuery().
								Must(
									elastic.NewMatchPhraseQuery("usage.text", partText),
									elastic.NewTermQuery("usage.text.words_count", numberOfWords),
									elastic.NewRangeQuery("usage.shared").Lt(max),
								),
						),
				),
				elastic.NewBoolQuery().
					MustNot(
						elastic.NewNestedQuery(
							"usage",
							elastic.NewExistsQuery("usage"),
						),
					),
			)
	}

	return elastic.NewBoolQuery().
		Should(
			elastic.NewNestedQuery(
				"usage",
				elastic.NewFunctionScoreQuery().
					ScoreMode("first").
					BoostMode("sum").
					AddScoreFunc(
						elastic.NewFieldValueFactorFunction().
							Field("usage.shared").
							Factor(1).
							Modifier("none").
							Missing(0)).
					Query(
						elastic.NewBoolQuery().
							Must(
								elastic.NewMatchPhraseQuery("usage.text", partText),
								elastic.NewTermQuery("usage.text.words_count", numberOfWords),
								elastic.NewRangeQuery("usage.shared").Gte(min).Lt(max),
							),
					),
			),
		)

}

func getNumberOfSplitParts(partText string, playlistID string) *elastic.SearchRequest {

	return numberOfMatches(splitPartQuery(partText).Must(elastic.NewTermQuery("playlist.id", playlistID)))
}

func getNumberOfNonSplitParts(partText string, playlistID string) *elastic.SearchRequest {

	return numberOfMatches(nonSplitPartQuery(partText).Must(elastic.NewTermQuery("playlist.id", playlistID)))
}

//func (esi *Elastic) CreateAnnotationIndex(name string) error {
//
//	err := elastic6.CreateAnnotationIndex(name, esi.client)
//
//	if err != nil {
//		return err
//	}
//
//	esi.Index = name
//	return nil
//}

// AddAnnotations indexes multiple Annotations
func (esi *Elastic) AddAnnotations(annotations []*annotation.Annotation) error {

	return elastic6.AddAnnotations(annotations, esi.Index, esi.client)
}

// DeleteIndex deletes the configured index of Elastic
func (esi *Elastic) DeleteIndex() error {
	log.Debugf("Delete Index %s", esi.Index)

	response, err := esi.client.DeleteIndex(esi.Index).Do(context.Background())
	if err != nil {
		return err
	}

	if !response.Acknowledged {
		return fmt.Errorf("index %s not achnowledged", esi.Index)
	}

	return nil
}

// FindByID searches and retrieves an Annotation with _id = id
func (esi *Elastic) FindByID(id string) (*annotation.Annotation, error) {
	defer monitor.Elapsed()()

	result, err := esi.client.Search().
		Index(esi.Index).
		Query(elastic.NewTermQuery("_id", id)).
		From(0).Size(1).
		Do(context.Background())

	if err != nil {
		return nil, err
	}

	if result.TotalHits() != 1 {
		return nil, fmt.Errorf("no document found with the _id: %s", id)
	}

	a, err := unmarshalToAnnotation(result.Hits.Hits[0])
	if err != nil {
		return nil, err
	}

	return a, nil

}

// Playlists returns an array of all playlists
func (esi *Elastic) Playlists() (pls []annotation.Playlist, err error) {
	defer monitor.Elapsed()()

	result, err := esi.client.Search().
		Index(esi.Index).
		Aggregation("playlists", elastic.NewCompositeAggregation().
			Sources(
				elastic.NewCompositeAggregationTermsValuesSource("id").Field("playlist.id"),
				elastic.NewCompositeAggregationTermsValuesSource("name").Field("playlist.name"),
			)).
		Do(context.Background())

	if err != nil {
		return nil, err
	}

	item, ok := result.Aggregations.Composite("playlists")

	if ok != true {
		return nil, errors.New("Aggregations.Composite playlists is empty")
	}

	for i := range item.Buckets {

		pls = append(pls, annotation.Playlist{
			ID:   item.Buckets[i].Key["id"].(string),
			Name: item.Buckets[i].Key["name"].(string),
		})

	}

	return pls, err

}
