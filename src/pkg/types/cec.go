package types

import (
	"gitlab.ethz.ch/chgerber/MessageComposition/src/pkg/util"
	"strconv"
)

// CandidateEquivalenceClass (CEC) represents an equivalence class which groups candidates with the same base-3 representation.
type CandidateEquivalenceClass struct {
	ID         int // base3 representation
	PECs       []*PartEquivalenceClass
	TargetText string
}

// Splits returns the SplitRange of the CEC
// (0,2)-range means that a candidate of this CEC with minimal 0 split pecs or maximal 2 split pecs is available.
// (3,3)-range means that only candidates of this CEC with 3 split pecs are available.
func (cec *CandidateEquivalenceClass) Splits() SplitRange {
	min := 0
	max := 0
	for _, part := range cec.PECs {
		min += part.Splits().Min
		max += part.Splits().Max
	}
	return SplitRange{min, max}
}

// Base3 returns the base-3 representation of the CEC.
// Each CEC has a unique base-3 representation. The base-3 representation is always with respect to the inputQuery.
// The base-3 representations of CEC for a query of length n words has n digits.
//
// Meaning of the value of ith-digit of the base-3 representation:
// 1 -> ith-word begins a new present part.
// 2 -> ith- word begins a new missing part.
// 0 -> ith-word belongs to current part.
//
// e.g. Input query: "my name is arni"
// A CEC with two pecs: part-1: "my name" part-2: "arni" has the base-3 representation: 1021
func (cec *CandidateEquivalenceClass) Base3() string {
	return strconv.FormatInt(int64(cec.ID), 3)
}

// PartFreq returns the sequence of number of available pecs of the corresponding part equivalence classes in the CEC.
func (cec *CandidateEquivalenceClass) PartFreq() []int {
	var freq []int
	for _, part := range cec.PECs {
		freq = append(freq, part.Freq())
	}
	return freq
}

// SplitFreq returns the sequence of number of available pecs of the corresponding part equivalence classes in the CEC partitioned in split pecs and non-split pecs.
// e.g, returned value [[2, 3],[5, 7], [0, 3]] means that of the first part equivalence class 2 non-split and 3 split pecs are available. The third part equivalence class accounts for 0 non-split pecs and 3 split pecs.
func (cec *CandidateEquivalenceClass) SplitFreq() [][]int {
	var splitFreq [][]int
	for _, part := range cec.PECs {
		splitFreq = append(splitFreq, []int{part.NotSplitPartFreq, part.SplitPartFreq})
	}
	return splitFreq
}

// PartLengths returns the sequence of the text lengths of the part equivalence classes.
func (cec *CandidateEquivalenceClass) PartLengths() []int {
	var lengths []int
	for _, part := range cec.PECs {
		lengths = append(lengths, part.Length())
	}
	return lengths
}

// NumPEC returns the number of pecs all candidates of this CEC are made of.
func (cec *CandidateEquivalenceClass) NumPEC() int {
	return len(cec.PECs)
}

// Text returns the text that results when the text of all PEC is concatenated with space delimitation.
func (cec *CandidateEquivalenceClass) Text() string {
	var partTexts []string
	for _, part := range cec.PECs {
		partTexts = append(partTexts, part.Text)
	}
	return util.WordsToString(partTexts)
}

// NumCandidates returns the number of distinct Candidates that are available of this CEC.
func (cec *CandidateEquivalenceClass) NumCandidates() int {
	numRealizations := 0
	for i := cec.Splits().Min; i <= cec.Splits().Max; i++ {
		num, _ := cec.NumCandidatesWithNSplitParts(i)
		numRealizations += num
	}
	return numRealizations
}

// NumCandidatesWithNSplitParts returns the total number of candidates with exactly n split parts that are available of this CEC.
// splitPos is a set of base-3 numbers of length splitKind.NumPEC indicating with a value 1 which parts are the split parts
// e.g. when NumCandidatesWithNSplitParts(1) return with (15, ["010", "100"]) it means that 15 candidates with one split part are available and that either the first or the second part is the split part for all 15 candidates.
func (cec *CandidateEquivalenceClass) NumCandidatesWithNSplitParts(n int) (total int, splitPos []string) {
	numTotal := 0
	var splitPositionsBase2 []string
	splitLocations := util.BinaryNumbersWithKones(n, cec.NumPEC())
	for _, splitLocation := range splitLocations {
		num := 1
		for i := 0; i < cec.NumPEC(); i++ {
			if splitLocation[i] == '1' {
				num *= cec.SplitFreq()[i][1]
			} else {
				num *= cec.SplitFreq()[i][0]
			}
			if num == 0 {
				break
			}
		}
		numTotal += num
		if num > 0 {
			splitPositionsBase2 = append(splitPositionsBase2, splitLocation)
		}
	}
	return numTotal, splitPositionsBase2
}

// SplitRange specifies what's the minimum and maximum number of splits of the candidates
type SplitRange struct {
	Min int
	Max int
}

// ScoredCEC extends CEC with the information that are missed to be scored by score.Referee
type ScoredCEC struct {
	*CandidateEquivalenceClass
	Score      float64
	Splits     int
	Popularity int
}

// MoreLowerScoreLevels returns true if there are more (lower) score levels left for the given ScoredCEC
func (sCEC *ScoredCEC) MoreLowerScoreLevels() bool {
	if sCEC.Splits < sCEC.NumPEC() {

		return true
	} else if sCEC.Popularity > 0 {

		return true
	}

	return false
}

// Target returns the target text
func (sCEC *ScoredCEC) Target() string {
	return sCEC.TargetText
}

// NumParts returns the number of parts this type of cec has
func (sCEC *ScoredCEC) NumParts() int {
	return sCEC.NumPEC()
}

// NumSplits returns the number of splits
func (sCEC *ScoredCEC) NumSplits() int {
	return sCEC.Splits
}

// PopularityLevel returns the popularity level
func (sCEC *ScoredCEC) PopularityLevel() int {
	return sCEC.Popularity
}

// NumCandidates returns the number of Candidates that can be realized
func (sCEC *ScoredCEC) NumCandidates(popScoreBase int) (int, error) {

	popularityLevelStrings, err := util.FindSumOfDigitNumbers(sCEC.NumParts(), sCEC.Popularity, popScoreBase)
	if err != nil {
		return 0, err
	}

	splitLocationStrings := util.BinaryNumbersWithKones(sCEC.Splits, sCEC.NumPEC())

	numTotal := 0

	for _, popularityLevelString := range popularityLevelStrings {

		for _, splitLocationString := range splitLocationStrings {

			num := 1
			for i := 0; i < sCEC.NumPEC(); i++ {

				s := string(popularityLevelString[i])
				level, err := strconv.ParseInt(s, popScoreBase, 0)
				if err != nil {
					return 0, err
				}

				if splitLocationString[i] == '1' {

					num *= sCEC.PECs[i].PartFrequency[true][int(level)]

				} else {
					num *= sCEC.PECs[i].PartFrequency[false][int(level)]
				}

				if num == 0 {
					break
				}

			}
			numTotal += num

		}
	}
	return numTotal, nil

}
