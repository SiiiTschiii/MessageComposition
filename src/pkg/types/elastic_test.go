package types

import (
	"context"
	"errors"
	"github.com/olivere/elastic"
	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"os"
	"strings"
	"testing"
)

var es *Elastic

func TestMain(m *testing.M) {

	log.SetLevel(log.WarnLevel)

	err := errors.New("")
	es, err = NewElastic(os.Getenv("ES_URL_TEST"), "dev.test")
	if err != nil {
		log.Error(err)
	}

	os.Exit(m.Run())
}

func TestGetNumberOfParts(t *testing.T) {
	nonSplit, split, err := es.GetNumberOfParts("test", "thank you")
	assert.Equal(t, nil, err)
	assert.Equal(t, 38, nonSplit[0])
	assert.Equal(t, 3, split[0])

	nonSplit, split, err = es.GetNumberOfParts("test", "thank you", "thank", "you")
	assert.Equal(t, nil, err)
	assert.Equal(t, 612, nonSplit[2])
	assert.Equal(t, 17, split[2])
}

func TestRetrieveDocs(t *testing.T) {
	result, err := es.retrieveDocs(nonSplitPartQuery("thank you"), 38, elastic.NewHighlight().Field("subtitle.text"))
	assert.Equal(t, nil, err)

	assert.Equal(t, 38, len(result.Hits.Hits))

	// test for hihlighting information
	for _, hit := range result.Hits.Hits {

		_, ok := hit.Highlight["subtitle.text"]
		assert.Equal(t, true, ok)
	}

	result, err = es.retrieveDocs(splitPartQuery("thank you"), 2, elastic.NewHighlight().Field("subtitle.text"))
	assert.Equal(t, nil, err)

	assert.Equal(t, 2, len(result.Hits.Hits))

	for _, hit := range result.Hits.Hits {
		_, ok := hit.Highlight["subtitle.text"]
		assert.Equal(t, true, ok)
	}

}

func TestElastic_RetrieveNonSplitParts(t *testing.T) {
	parts, _, err := es.RetrieveNonSplitParts("thank you", 38, "test")
	assert.Equal(t, nil, err)
	assert.Equal(t, 38, len(parts))

	for _, part := range parts {
		assert.Equal(t, true, part.Text == "thank you")
		assert.Equal(t, false, part.Split)

		assert.Equal(t, true, strings.Contains(part.Highlight, "<em>"))
		assert.Equal(t, true, strings.Contains(part.Highlight, "</em>"))
	}
}

func TestElastic_RetrieveSplitParts(t *testing.T) {
	parts, _, err := es.RetrieveSplitParts("thank you", 2, "test")
	assert.Equal(t, nil, err)
	assert.Equal(t, 2, len(parts))

	for _, part := range parts {
		assert.Equal(t, true, part.Text == "thank you")
		assert.Equal(t, true, part.Split)

		assert.Equal(t, true, strings.Contains(part.Highlight, "<em>"))
		assert.Equal(t, true, strings.Contains(part.Highlight, "</em>"))
	}
}

func TestElastic_RetrieveNonSplitPartPopularityRange(t *testing.T) {
	parts, _, err := es.RetrieveNonSplitPartsRange("ever", 100, int64(10), int64(20), "test")
	assert.Equal(t, nil, err)
	assert.Equal(t, 1, len(parts))

	parts, _, err = es.RetrieveNonSplitPartsRange("ever", 100, int64(0), int64(20), "test")
	assert.Equal(t, nil, err)
	assert.Equal(t, 23, len(parts))

	parts, _, err = es.RetrieveNonSplitParts("ever", 100, "test")
	assert.Equal(t, nil, err)
	assert.Equal(t, 23, len(parts))

	parts, _, err = es.RetrieveNonSplitPartsRange("ever", 100, int64(1e4), int64(1e5), "test")
	assert.Equal(t, nil, err)
	assert.Equal(t, 0, len(parts))

}

func TestElastic_TestHighlight(t *testing.T) {
	parts, _, err := es.RetrieveNonSplitParts("wet noodle", 1, "test")
	assert.Equal(t, nil, err)
	assert.Equal(t, 1, len(parts))

	for _, part := range parts {
		assert.Equal(t, true, part.Text == "wet noodle")

		assert.Equal(t, "wet noodle", parts[0].ExtractHighlight())
	}
}

func TestElastic_FindByID(t *testing.T) {
	a, err := es.FindByID("5d317cc4ed5f48315275c130")
	assert.Equal(t, nil, err)
	assert.Equal(t, "5d317cc4ed5f48315275c130", a.ID.Hex())
}

func TestElastic_RetrievePartById(t *testing.T) {
	nonSplitPart, err := es.RetrievePartByID("5d317ccaed5f48315275c1b1", "whatsapp it")
	assert.Equal(t, nil, err)
	assert.Equal(t, "5d317ccaed5f48315275c1b1", nonSplitPart.Doc.ID.Hex())
	assert.Equal(t, false, nonSplitPart.Split)

	splitPart, err := es.RetrievePartByID("5d317d0bed5f48315275cde7", "this is")
	assert.Equal(t, nil, err)
	assert.Equal(t, "5d317d0bed5f48315275cde7", splitPart.Doc.ID.Hex())
	assert.Equal(t, true, splitPart.Split)

}

func TestElastic_MultiSearch(t *testing.T) {

	var requests []*elastic.SearchRequest

	requests = append(requests, elastic.NewSearchRequest().Query(nonSplitPartQuery("thank you")))
	requests = append(requests, elastic.NewSearchRequest().Query(splitPartQuery("thank you")))

	result, err := es.client.MultiSearch().Index("dev.test").Add(requests...).Do(context.Background())
	assert.Equal(t, nil, err)
	assert.Equal(t, 2, len(result.Responses))

	assert.Equal(t, int64(38), result.Responses[0].TotalHits())
	assert.Equal(t, int64(3), result.Responses[1].TotalHits())

}

func TestPlaylist(t *testing.T) {
	nonSplit, split, err := es.GetNumberOfParts("other", "here")
	assert.Equal(t, nil, err)
	assert.Equal(t, 1, nonSplit[0])
	assert.Equal(t, 0, split[0])
}

func TestListPlaylists(t *testing.T) {
	playlists, err := es.Playlists()
	assert.Equal(t, nil, err)
	assert.Equal(t, 2, len(playlists))
	assert.Equal(t, "other", playlists[0].Name)
	assert.Equal(t, "other", playlists[0].ID)
	assert.Equal(t, "test", playlists[1].Name)
	assert.Equal(t, "test", playlists[1].ID)

}

var result *elastic.SearchResult

func benchmarkElasticSplitPartQuery(query string, b *testing.B) {

	es, err := NewElastic(
		os.Getenv("ES_URL_TEST"),
		"dev.1m2",
	)

	if err != nil {
		b.Fatal(err)
	}

	var elasticServerDuration int64
	var numDocs int64

	b.ResetTimer()

	for n := 0; n < b.N; n++ {

		result, err = es.client.Search().
			Index(es.Index).
			Query(splitPartQuery(query)).
			From(0).Size(0).
			Do(context.Background())

		if err != nil {
			b.Fatal(err)
		}

		elasticServerDuration += result.TookInMillis
		numDocs += result.TotalHits()

	}

	log.Debugf("elasticServerDuration: %v", float64(elasticServerDuration)/float64(b.N))
	log.Debugf("numDocsFound: %v", numDocs/int64(b.N))

}

func BenchmarkElasticSplitPartQueryThe(b *testing.B) {
	benchmarkElasticSplitPartQuery("the", b)
}
func BenchmarkElasticSplitPartQueryYou(b *testing.B) {
	benchmarkElasticSplitPartQuery("you", b)
}
func BenchmarkElasticSplitPartQueryThankYou(b *testing.B) {
	benchmarkElasticSplitPartQuery("thank you", b)
}
func BenchmarkElasticSplitPartQueryHowDoesThatSound(b *testing.B) {
	benchmarkElasticSplitPartQuery("how does that sound", b)
}

func benchmarkElasticNonSplitPartQuery(query string, b *testing.B) {

	es, err := NewElastic(
		os.Getenv("ES_URL_TEST"),
		"dev.1m2",
	)

	if err != nil {
		b.Fatal(err)
	}

	var elasticServerDuration int64
	var numDocs int64

	b.ResetTimer()

	for n := 0; n < b.N; n++ {

		result, err = es.client.Search().
			Index(es.Index).
			Query(nonSplitPartQuery(query)).
			From(0).Size(0).
			Do(context.Background())

		if err != nil {
			b.Fatal(err)
		}

		elasticServerDuration += result.TookInMillis
		numDocs += result.TotalHits()

	}

	log.Debugf("elasticServerDuration: %v", float64(elasticServerDuration)/float64(b.N))
	log.Debugf("numDocsFound: %v", numDocs/int64(b.N))

}

func BenchmarkElasticNonSplitPartQueryThe(b *testing.B) {
	benchmarkElasticNonSplitPartQuery("the", b)
}
func BenchmarkElasticNonSplitPartQueryYou(b *testing.B) {
	benchmarkElasticNonSplitPartQuery("you", b)
}
func BenchmarkElasticNonSplitPartQueryThankYou(b *testing.B) {
	benchmarkElasticNonSplitPartQuery("thank you", b)
}
func BenchmarkElasticNonSplitPartQueryHowDoeasThatSound(b *testing.B) {
	benchmarkElasticNonSplitPartQuery("how does that sound", b)
}
