package types

import (
	log "github.com/sirupsen/logrus"
	"gitlab.ethz.ch/chgerber/annotation/v2"
	"strings"
)

// Part represents a piece of a subtitle.
// A part always refers to an input query (target text)
type Part struct {
	Text string
	// Split is true if the part is a split part.
	// A split part is a part whose text is only a substring of the underlying subtitle text.
	Split bool
	Doc   *annotation.Annotation
	// subtitle.text field with Text enclosed by <em> </em>
	Highlight string
}

// PartLength returns the length of the part text.
// Length is the number of space delimited words.
func (p *Part) PartLength() int {
	return len(strings.Fields(p.Text))
}

// ExtractHighlight returns the first occurence of text in the Highlight field, which contains a string with all matched terms enclosed with <em>, </em>
// e.g. "oh <em>thank</em> <em>you</em> <em>thank</em> <em>you</em> man"
func (p *Part) ExtractHighlight() (text string) {

	segments := strings.SplitAfter(p.Highlight, "</em>")
	var terms []string
	// TODO To take PartLength() as number of segments number might not always be true
	for i := 0; i < p.PartLength(); i++ {
		term := extractXMLTags("<em>", "</em>", segments[i])
		terms = append(terms, term)
	}

	return strings.Join(terms, " ")
}

// extractXMLTags extracts the first tag value
// "oh <em>thank</em> <em>you</em> --> returns "thank"
func extractXMLTags(startTag string, endTag string, text string) string {
	startIdx := strings.Index(text, startTag) + len(startTag)
	endIdx := strings.Index(text, endTag) - 1
	return text[startIdx : endIdx+1]
}

// StartTime returns the start time of the part.Text in the subtitle.
func (p *Part) StartTime() int {

	// splitPart has no segment timing
	if p.Split {
		return p.Doc.Subtitle.Start
	}

	// nonSplit without segmnets -> perfect match
	if p.Doc.Subtitle.Segments == nil {
		return p.Doc.Subtitle.Start
	}

	// deduce time from segments
	text := p.ExtractHighlight()
	startSegmentIdx, _, err := p.Doc.Subtitle.FindInSegments(text)
	if err != nil {
		log.Warn(err)
		return p.Doc.Subtitle.Start
	}

	return p.Doc.Subtitle.Segments[startSegmentIdx].Start
}

// EndTime returns the end time of the part.Text in the subtitle.
func (p *Part) EndTime() int {

	// splitPart has no segment timing
	if p.Split {
		return p.Doc.Subtitle.End
	}

	// nonSplit without segmnets -> perfect match
	if p.Doc.Subtitle.Segments == nil {
		return p.Doc.Subtitle.End
	}

	// deduce time from segments
	text := p.ExtractHighlight()
	_, stopSegmentIdx, err := p.Doc.Subtitle.FindInSegments(text)
	if err != nil {
		log.Warn(err)
		return p.Doc.Subtitle.End
	}

	return p.Doc.Subtitle.Segments[stopSegmentIdx].End
}
