package types

import (
	"strings"
)

// Candidate represents one result of the message composition task.
// A candidate always refers to an input phrase query (= target text)
type Candidate struct {
	TargetText string
	Parts      []Part
	Score      float64
	CEC        *CandidateEquivalenceClass
	Popularity int
}

// DocIDs returns the document id of all part documents
func (c *Candidate) DocIDs() (docIDs []string) {
	for _, part := range c.Parts {
		docIDs = append(docIDs, part.Doc.ID.Hex())
	}
	return docIDs
}

// Base3 returns the base3 representation of the candidate
func (c *Candidate) Base3() string {
	if c.CEC == nil {
		return ""
	}
	return c.CEC.Base3()
}

// NumParts returns the number of parts the candidate consists of.
func (c *Candidate) NumParts() int {
	return len(c.Parts)
}

// PartLengths returns the lengths of the parts in order of occurrence in the candidate.
func (c *Candidate) PartLengths() []int {
	var partLengths []int
	for _, part := range c.Parts {
		partLengths = append(partLengths, part.PartLength())
	}
	return partLengths
}

// Text concatenates the text of all parts delimited by a space.
// Note that Text() needn't result in the same as Target(). Only if the candidate is a complete candidate (max similarity score).
func (c *Candidate) Text() string {
	delimiter := " "
	var text string
	for _, part := range c.Parts {
		text += part.Text + delimiter
	}
	text = strings.TrimSuffix(text, delimiter)
	return text
}

// NumSplits returns the number of split parts of the candidate.
// A split part is a part whose text is only a substring of the underlying subtitle text.
func (c *Candidate) NumSplits() int {
	numSplits := 0
	for _, part := range c.Parts {
		if part.Split {
			numSplits++
		}
	}
	return numSplits
}

// Target implements the videorecipe.VideoCandidate interface
func (c *Candidate) Target() string {
	return c.TargetText
}

// PopularityLevel returns the popularity level of the candidate
func (c *Candidate) PopularityLevel() int {
	return c.Popularity
}
