package types

// PartEquivalenceClass (PECs) is an equivalence class to which all PECs which represent the same substring of the input query (target text) belong to.
// PartEquivalenceClass always refers to a specific input phrase query (target text).
type PartEquivalenceClass struct {
	//Text is the Part text this PECs represents
	Text string
	// StartIDX is the word number, starting with 0, of the input query where the text of this PECs starts.
	StartIDX int
	// EndIDX is the word number, starting with 0, of the input query where the text of this PECs ends.
	EndIDX int
	// SplitPartFreq is the number of split pecs that exist of the given PECs.
	SplitPartFreq int
	// NotSplitPartFreq is the number of non split pecs of the given PECs.
	NotSplitPartFreq int

	PartFrequency map[bool]map[int]int

	// Parts is multidimensional map that contains the parts of this CEC.
	// boolean map -> split or non/split
	// int map -> popularity level
	Parts map[bool]map[int][]*Part
}

// Length returns length of the part text in number of space delimited words.
func (pec *PartEquivalenceClass) Length() int {
	return pec.EndIDX - pec.StartIDX + 1
}

// Splits returns the range of available part types of the given PECs.
// (0, 1)-range means both split pecs and non-split pecs are available.
// (1, 1)-range means only split pecs are available.
// (0, 0)-range means no split pecs are available.
func (pec *PartEquivalenceClass) Splits() SplitRange {
	if pec.SplitPartFreq > 0 {
		if pec.NotSplitPartFreq > 0 {
			return SplitRange{0, 1}
		}
		return SplitRange{1, 1}
	}
	return SplitRange{0, 0}
}

//Freq returns the number of parts that are available for the given PECs.
func (pec *PartEquivalenceClass) Freq() int {
	return pec.NotSplitPartFreq + pec.SplitPartFreq
}
