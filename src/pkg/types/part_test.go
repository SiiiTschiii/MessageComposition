package types

import (
	"github.com/stretchr/testify/assert"
	"gitlab.ethz.ch/chgerber/annotation/v2"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"testing"
)

func TestCreatePart(t *testing.T) {
	part := Part{
		Text:  "is arni",
		Split: true,
		Doc: &annotation.Annotation{
			ID: primitive.NewObjectID(),
			Subtitle: annotation.Subtitle{
				Text: "my name is arni",
			},
		},
	}

	expS := "is arni"
	if is := part.Text; is != expS {
		t.Errorf("Expected '%s' got '%s'", expS, is)
	}
}

func TestPart_SplitNeeded(t *testing.T) {
	part := Part{
		Text:  "is arni",
		Split: true,
		Doc: &annotation.Annotation{
			ID: primitive.NewObjectID(),
			Subtitle: annotation.Subtitle{
				Text: "my name is arni",
			},
		},
	}

	if is := part.Split; is != true {
		t.Errorf("Expectet '%t' got '%t'", true, is)
	}
}

func TestPart_ExtractHighlight(t *testing.T) {
	part := Part{
		Text:  "is arni",
		Split: true,
		Doc: &annotation.Annotation{
			Subtitle: annotation.Subtitle{
				Text: "my name is arni is arni dude",
			},
		},
		Highlight: "my name <em>is</em> <em>arni</em> <em>is</em> <em>arni</em> dude",
	}

	assert.Equal(t, "is arni", part.ExtractHighlight())
}

func TestPart_StartEndTime(t *testing.T) {
	part := Part{
		Text:  "is arni",
		Split: false,
		Doc: &annotation.Annotation{
			Subtitle: annotation.Subtitle{
				Text: "my name is arni is arni dude",
				Segments: []annotation.TimedText{
					{
						Start: 0,
						End:   1,
						Text:  "my",
					},
					{
						Start: 2,
						End:   3,
						Text:  "name",
					},
					{
						Start: 4,
						End:   5,
						Text:  "is",
					},
					{
						Start: 6,
						End:   7,
						Text:  "arni",
					},
					{
						Start: 8,
						End:   9,
						Text:  "is",
					},
					{
						Start: 10,
						End:   11,
						Text:  "arni",
					},
					{
						Start: 12,
						End:   13,
						Text:  "dude",
					},
				},
				Start: 0,
				End:   13,
			},
		},
		Highlight: "my name <em>is</em> <em>arni</em> <em>is</em> <em>arni</em> dude",
	}

	assert.Equal(t, 4, part.StartTime())
	assert.Equal(t, 7, part.EndTime())

	part = Part{
		Text:  "is arni",
		Split: true,
		Doc: &annotation.Annotation{
			Subtitle: annotation.Subtitle{
				Text:  "my name is arni is arni dude",
				Start: 0,
				End:   13,
			},
		},
		Highlight: "my name <em>is</em> <em>arni</em> <em>is</em> <em>arni</em> dude",
	}

	assert.Equal(t, 0, part.StartTime())
	assert.Equal(t, 13, part.EndTime())

	part = Part{
		Text:  "is arni",
		Split: false,
		Doc: &annotation.Annotation{
			Subtitle: annotation.Subtitle{
				Text:  "is arni",
				Start: 4,
				End:   7,
			},
		},
		Highlight: "<em>is</em> <em>arni</em>",
	}

	assert.Equal(t, 4, part.StartTime())
	assert.Equal(t, 7, part.EndTime())
}
