package types

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestBase3FromOnePart(t *testing.T) {
	cec := CandidateEquivalenceClass{ID: 1, PECs: nil}
	assert.Equal(t, "1", cec.Base3())
}

func TestPartReferencesAcrossCEC(t *testing.T) {

	pec := PartEquivalenceClass{}

	pec.Parts = make(map[bool]map[int][]*Part)
	pec.Parts[false] = make(map[int][]*Part)

	cec1 := CandidateEquivalenceClass{
		ID:   1,
		PECs: []*PartEquivalenceClass{&pec},
	}

	cec2 := CandidateEquivalenceClass{
		ID:   1,
		PECs: []*PartEquivalenceClass{&pec},
	}

	pec.Parts[false][0] = []*Part{{Text: "test test"}}

	assert.Equal(t, "test test", cec1.PECs[0].Parts[false][0][0].Text)
	assert.Equal(t, "test test", cec2.PECs[0].Parts[false][0][0].Text)

	cec1.PECs[0].Parts[false][0][0].Text = "hello"

	assert.Equal(t, "hello", cec1.PECs[0].Parts[false][0][0].Text)
	assert.Equal(t, "hello", cec2.PECs[0].Parts[false][0][0].Text)
}

func TestTypes_NumCandidates(t *testing.T) {

	sCEC := ScoredCEC{
		Splits:     1,
		Popularity: 4,
		CandidateEquivalenceClass: &CandidateEquivalenceClass{
			ID: 271,
			PECs: []*PartEquivalenceClass{
				{
					SplitPartFreq:    1,
					NotSplitPartFreq: 1,
					PartFrequency:    map[bool]map[int]int{true: {0: 1, 1: 1, 2: 1}, false: {0: 1, 1: 1, 2: 2}},
				},
				{
					SplitPartFreq:    1,
					NotSplitPartFreq: 1,
					PartFrequency:    map[bool]map[int]int{true: {0: 1, 1: 1, 2: 1}, false: {0: 1, 1: 1, 2: 1}},
				},
				{
					SplitPartFreq:    1,
					NotSplitPartFreq: 1,
					PartFrequency:    map[bool]map[int]int{true: {0: 1, 1: 1, 2: 1}, false: {0: 1, 1: 1, 2: 1}},
				},
			},
		},
	}

	num, err := sCEC.NumCandidates(3)
	assert.Equal(t, nil, err)
	assert.Equal(t, 24, num)

}
