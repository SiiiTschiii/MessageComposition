#!/bin/bash

logFile="$1/reencode.log"
fileNameAddon="-NoTempCompr"

for file in $1/*.$2; do

    newFile=${file%%.$2}
    newFile=$newFile"$fileNameAddon.$2"
    # don't repete reencodings
    if [ -f $newFile ]; then
      echo "$newFile already exists" >> $logFile
    elif [[ $file = *${fileNameAddon}* ]]; then
      echo "dont reencode reencoded files"
    else
      START_TIME=$SECONDS
      ffmpeg -hide_banner -i $file -vcodec libx264 -x264-params keyint=1:scenecut=0 $newFile
      ELAPSED_TIME=$(($SECONDS - $START_TIME))
      oldFileSize=$(stat --printf="%s" $file)
      newFileSize=$(stat --printf="%s" $newFile)
      oldFileName=$(basename $file)
      newFileName=$(basename $newFile)
      echo "Reencode $oldFileName $oldFileSize Bytes -> $newFileName $newFileSize Bytes in $ELAPSED_TIME seconds" >> $logFile
  fi
done
