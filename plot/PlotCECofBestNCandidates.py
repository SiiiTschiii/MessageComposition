#!/usr/bin/python3.6
from itertools import islice

import matplotlib.pyplot as plt
import pandas as pd

def main():
    # src: https://stackoverflow.com/questions/51272304/how-to-create-a-categorical-bubble-plot-in-python

    inputCSVfile = "/tmp/PlotCECofBestNCandidates.csv"
    outputPDFfile = "/tmp/PlotCECofBestNCandidates.pdf"

    # load title
    title = ""
    with open(inputCSVfile) as f:
        head = list(islice(f, 6))
        print(head)

    # load data
    df = pd.read_csv(inputCSVfile, skiprows=6)

    #create padding column from values for circles that are neither too small nor too large
    df["padd"] = 2.5 * (df.freq - df.freq.min()) / ((df.freq.max() - df.freq.min()) + 0.5)
    fig = plt.figure()

    s = plt.scatter(df.score, df.base3, s = 0)
    s.remove
    #plot data row-wise as text with circle radius according to Count
    for row in df.itertuples():
        bbox_props = dict(boxstyle = "circle, pad = {}".format(row.padd), fc = "w", ec = "r", lw = 2)
        plt.annotate(str(row.freq)+"-"+ str(row.splits)+"-"+str(row.popularity), xy = (row.score, row.base3), bbox = bbox_props, ha="center", va="center", zorder = 2, clip_on = True)

    #plot grid behind markers
    plt.grid(ls = "--", zorder = 1)
    #take care of long labels
    fig.autofmt_xdate()
    plt.tight_layout()

    plt.xlabel("Score")
    plt.ylabel("CEC [base-3 representation]")
    plt.title("Phrase: "+head[0])
    plt.figtext(1.0, 0.5, head[1]+head[2]+head[3]+head[4]+head[5], wrap=True,
                horizontalalignment='left', fontsize=12)
    #plt.show()

    fig.savefig(outputPDFfile, bbox_inches='tight')

if __name__ == "__main__":
    main()