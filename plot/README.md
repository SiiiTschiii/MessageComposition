# Plot

### Plot Scripts and their function

    .
    ├── idx_stats.ipynb                  # Plot the term frequency of a given index.
    ├── subs_stats.ipynb                 # Plot the term length distribution of a given positional index
    ├── PlotCECofBestNCandidates.py      # Plots a PDF with the CEC base-3 diagram of the given N candidates of a query.
