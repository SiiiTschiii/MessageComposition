# Message Composition

[![pipeline status](https://gitlab.ethz.ch/chgerber/MessageComposition/badges/master/pipeline.svg)](https://gitlab.ethz.ch/chgerber/MessageComposition/commits/master)
[![coverage report](https://gitlab.ethz.ch/chgerber/MessageComposition/badges/master/coverage.svg)](https://gitlab.ethz.ch/chgerber/MessageComposition/commits/master)

Composing Messages from Movie Subtitles

## Go Setup with GVM

##### Initially
```bash
# gvm golang version management
gvm install go1.4 -B
gvm use go1.4
export GOROOT_BOOTSTRAP=$GOROOT
gvm list  # list installed versions
gvm listall # list available versions
```

##### Update go version
```bash

gvm install go1.11.5
go use go.1.11.5 --default
# verify in new terminal
go version
```